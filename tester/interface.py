#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from docker_host import *
from scapy.all import *
import os
import time
import logging
import threading
import subprocess

load_contrib("ospf")
logger = logging.getLogger("tester.interface")


def stop_subprocess(proc):
    if proc.poll() is None:
        proc.terminate()
    return


class TestInterface(object):
    def __init__(self, iface, vlan=None):
        self.vlan = vlan
        self.sniffer = None
        self.__sniff_timer = None
        self.sniffed_pkts = []
        if vlan is not None:
            with open(os.devnull) as null:
                cmd = "ip link show {}.{}".format(iface, vlan)
                logger.debug("Execute: '{}'".format(cmd))
                check_iface = subprocess.call(cmd, shell=True, stdout=null,
                                              stderr=null)
                if check_iface != 0:
                    cmd = "ip link add link {0} name {0}.{1} " \
                          "type vlan id {1}".format(iface, vlan)
                    logger.debug("Execute: '{}'".format(cmd))
                    subprocess.check_call(cmd, shell=True, stdout=null,
                                          stderr=null)
                    filled_vlan_id = "{:04}".format(vlan)
                    new_mac = "22:33:44:55:{}:{}".format(filled_vlan_id[:2],
                                                         filled_vlan_id[2:])
                    mac = "ip link set dev {}.{} address {}".format(iface, vlan,
                                                                    new_mac)
                    logger.debug("Execute: '{}'".format(mac))
                    subprocess.check_call(mac, shell=True, stdout=null,
                                          stderr=null)
                    up = "ip link set dev {}.{} up mtu 2000".format(iface, vlan)
                    logger.debug("Execute: '{}'".format(up))
                    subprocess.check_call(up, shell=True, stdout=null,
                                          stderr=null)
            self.iface = "{}.{}".format(iface, vlan).encode("ascii")
        else:
            self.iface = iface.encode("ascii")
        logger.info("Created iface '{}'".format(self.iface))
        return

    def delete_subiface(self):
        if self.vlan is not None:
            logger.info("{} is removing subinterfaces...".format(self.iface))
            with open(os.devnull) as null:
                cmd = "ip link delete {} type vlan".format(self.iface)
                logger.debug("Execute: '{}'".format(cmd))
                subprocess.check_call(cmd, shell=True, stdout=null, stderr=null)
        return

    def send_frames(self, pkts, inter=0, count=1):
        """
        Sends scapy's packets on L2
        :param pkts: list of Scapy's packets to be sent
        :param inter: interval between packets in seconds
        :param count: send each packet `count` times
        :return:
        """
        pkts_to_sendp = []
        if count > 1:
            for _ in xrange(count):
                for pkt in pkts:
                    pkts_to_sendp.append(pkt.copy())
        else:
            pkts_to_sendp = pkts
        th = threading.Thread(target=sendp,
                              args=(pkts_to_sendp,),
                              kwargs={"iface": self.iface, "inter": inter,
                                      "verbose": 0})
        logger.info("'{}' is sending {} pkts".format(self.iface,
                                                         len(pkts_to_sendp)))
        th.start()
        return

    def start_sniffer(self, bpf_filter="", count=0, timeout=0):
        if self.sniffer is not None:
            err_msg = "Sniffer on {} is already working".format(self.iface)
            logger.error(err_msg)
            raise RuntimeError(err_msg)
        if not (count or timeout):
            logger.warning("Sniffer on '{}' is starting "
                           "without limitations".format(self.iface))
        self.sniffed_pkts = []
        cmd = [
            "/usr/sbin/tcpdump",
            "-i",
            self.iface,
            "-w",
            "-",
            bpf_filter,
        ]
        if count > 0:
            cmd.insert(-1, "-c")
            cmd.insert(-1, str(count))
        logger.debug("Execute: '{}'".format(" ".join(cmd)))
        self.sniffer = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE)
        if timeout > 0:
            self.__sniff_timer = threading.Timer(timeout,
                                                 stop_subprocess,
                                                 args=(self.sniffer,))
            self.__sniff_timer.start()
            logger.debug("Timer started for '{}' seconds".format(timeout))
        # without sleep first packet is missed :( fix it, please
        time.sleep(0.1)
        logger.info("Sniffer on '{}' started".format(self.iface))
        return

    def stop_sniffer(self):
        stop_subprocess(self.sniffer)
        self.wait_for_sniffer()
        return

    def wait_for_sniffer(self):
        if self.sniffer is None:
            return
        try:
            while self.sniffer.poll() is None:
                time.sleep(0.1)
        except KeyboardInterrupt:
            if self.__sniff_timer:
                self.__sniff_timer.cancel()
            raise
        if self.__sniff_timer:
            self.__sniff_timer.cancel()
        ret = self.sniffer.returncode
        if ret < 0:
            err_msg = "tcpdump received signal {} and exited".format(ret)
            logger.error(err_msg)
            raise RuntimeError(err_msg)
        if ret > 0:
            tcpdump_error = self.sniffer.stderr.read()
            err_msg = "tcpdump had an error: '{}'".format(tcpdump_error)
            logger.error(err_msg)
            raise RuntimeError(err_msg)
        logger.info("Sniffer on '{}' stopped".format(self.iface))
        pkts = rdpcap(self.sniffer.stdout)
        logger.debug("Sniffer on '{}' got '{}' pkts".format(self.iface,
                                                           len(pkts)))
        self.sniffed_pkts = list(pkts)
        self.sniffer = None
        self.__sniff_timer = None
        return

    def add_docker_bridge(self):
        name = "br{}".format(self.iface)
        logger.debug("Adding Docker bridged network".format(name))
        cmd = "docker network ls -q -f name={}".format(name)
        logger.debug("Execute: '{}'".format(cmd))
        net = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        if net:
            logger.debug("Docker bridge '{}' already exists".format(name))
            return net
        cmd = "docker network create -d bridge {}".format(name)
        logger.debug("Execute: '{}'".format(cmd))
        br_hash = subprocess.check_output(cmd, shell=True,
                                          stderr=subprocess.STDOUT)
        return br_hash

    def bridge_to_docker(self):
        logger.debug("Adding '{}' to Docker bridged network".format(self.iface))
        net_name = "br{}".format(self.iface)
        br_id = self.add_docker_bridge()
        br_name = "br-{}".format(br_id[:12])
        cmd = "brctl show {}".format(br_name)
        logger.debug("Execute: '{}'".format(cmd))
        br_ifaces = subprocess.check_output(cmd, shell=True,
                                            stderr=subprocess.STDOUT)
        if self.iface in br_ifaces:
            logger.debug("'{}' is already in Docker bridge".format(self.iface))
            return net_name
        with open(os.devnull) as null:
            cmd = "brctl addif {} {}".format(br_name, self.iface)
            logger.debug("Execute: '{}'".format(cmd))
            subprocess.check_call(cmd, shell=True, stdout=null, stderr=null)
        logger.debug("'{}' added in Docker bridge".format(self.iface))
        return net_name

    def dhcpv4_server(self, config_file, mac, *args):
        net = self.bridge_to_docker()
        volume = "{}:/etc/dhcp/dhcpd.conf".format(config_file)
        interfaces = ""
        for arg in args:
            interfaces += '"{}" '.format(arg)
        return DHCPv4Server(net=net, volume=volume, args=interfaces, mac=mac)

    def dhcpv4_client(self, outer_vlan=0, inner_vlan=0, mac=None, opt82=None):
        net = self.bridge_to_docker()
        args = "{} {}".format(outer_vlan, inner_vlan)
        if opt82 is not None:
            args += " -x 82:{}".format(opt82)
        return DHCPv4Client(net=net, args=args, mac=mac)

    def pppoe_server(self, mac, *args):
        net = self.bridge_to_docker()
        interfaces = ""
        for arg in args:
            interfaces += '"{}" '.format(arg)
        return PPPoEServer(net=net, privileged=True, args=interfaces, mac=mac)

    def pppoe_client(self, outer_vlan=0, inner_vlan=0, mac=None):
        net = self.bridge_to_docker()
        args = "{} {}".format(outer_vlan, inner_vlan)
        return PPPoEClient(net=net, privileged=True, args=args, mac=mac)

    def ipv4_client(self, ip_src, outer_vlan=0, inner_vlan=0, mac=None):
        net = self.bridge_to_docker()
        args = "{} {} {}".format(outer_vlan, inner_vlan, ip_src)
        return StaticIPv4Client(net=net, args=args, mac=mac)
