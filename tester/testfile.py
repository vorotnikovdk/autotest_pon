#!/usr/bin env python
# -*- coding: utf-8 -*-
""" Module for simple working with test files and dirs: conifgs, fw, etc."""
from __future__ import print_function
from __future__ import unicode_literals
import os
import shutil
import string
import tempfile
import logging

logger = logging.getLogger("tester.testfile")


class TestFile(object):
    def __init__(self, path, url_fmt):
        logger.info("Opened TestFile '{}'".format(path))
        self.path = self._original_path = path
        self._file = open(self.path, "a+t")
        os.chmod(self.path, 0777)
        self.url_fmt = url_fmt

    def read(self):
        self._file.seek(0)
        return self._file.read()

    def readlines(self):
        self._file.seek(0)
        return [line.rstrip() for line in self._file.readlines()]

    def _new_tempfile(self):
        if self.path == self._original_path:
            content = self.read()
        else:
            with open(self._original_path, "r") as original_file:
                content = original_file.read()
        self._file.close()
        file_dir, file_name = os.path.split(self._original_path)
        file_name += "."
        self._file = tempfile.NamedTemporaryFile(prefix=file_name, dir=file_dir,
                                                 mode="w+t")
        self.path = self._file.name
        os.chmod(self.path, 0777)
        self._file.write(content)
        self._file.seek(0)
        logger.info("Created TestFile copy '{}'".format(self.path))
        return

    def fill(self, **kwargs):
        self._new_tempfile()
        template = string.Template(self._file.read())
        self._file.seek(0)
        self._file.truncate()
        self._file.write(template.substitute(**kwargs))
        return

    def get_url(self, proto=None):
        return self.url_fmt.get_url(self.path, proto=proto)

    def copy_to(self, path):
        shutil.copy2(self.path, path)
        return

    def delete(self):
        if self.path != self._original_path:
            os.unlink(self._original_path)
            logger.info("Removed file '{}'".format(self._original_path))
        os.unlink(self.path)
        logger.info("Removed file '{}'".format(self.path))


class TestDir(object):
    def __init__(self, path, url_fmt):
        self.path = path
        logger.info("Opened TestFile '{}'".format(path))
        self.url_fmt = url_fmt

    def list(self):
        return os.listdir(self.path)

    def clear(self):
        for dir_file in self.list():
            dir_path = os.path.join(self.path, dir_file)
            logger.info("Removed file '{}'".format(dir_path))
            os.unlink(dir_path)

    def get_url(self, proto=None):
        return self.url_fmt.get_url(self.path, proto=proto)


def create_url_formatter(tester, device):
    if (("LTP" in device.hardware or "MA4000" in device.hardware) and
            device.software.startswith("3.")):
        url_fmt = OLT3xURLFormatter(tester)
    else:
        url_fmt = URLFormatter(tester)
    return url_fmt


class URLFormatter(object):
    def __init__(self, tester):
        self.tester = tester
        self.default_proto = "tftp"
        return

    def get_url(self, path, proto=None):
        file_dir, file_name = os.path.split(path)
        file_dir = file_dir.replace(self.tester.files, "")
        path = os.path.join(file_dir, file_name)
        if not proto:
            proto = self.default_proto
        if proto == "tftp":
            return self.get_tftp_url(path)
        elif proto == "http":
            return self.get_http_url(path)
        else:
            err_msg = "Unknown protocol '{}' for file '{}' ".format(proto, path)
            raise ValueError(err_msg)

    def get_tftp_url(self, path):
        raise ValueError("Protocol tftp isn't supported! File '{}".format(path))

    def get_http_url(self, path):
        raise ValueError("Protocol tftp isn't supported! File '{}".format(path))


class OLT3xURLFormatter(URLFormatter):
    def get_tftp_url(self, path):
        return "tftp://{}{}".format(self.tester.ip, path)

    def get_http_url(self, path):
        return "http://{}{}".format(self.tester.ip, path)
