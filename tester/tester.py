#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
import logging
import subprocess
from testfile import *
from interface import TestInterface

logger = logging.getLogger("tester.tester")


class Tester(object):
    def __init__(self, ip, file_dir, interfaces, tftp_port=69, http_port=80,
                 iptables="/sbin/iptables"):
        self.ip = ip
        logger.info("Tester file_dir '{}'".format(file_dir))
        self.files = file_dir
        self.tftp_port = int(tftp_port)
        self.http_port = int(http_port)
        self.iptables = iptables
        self.interfaces = {}
        logger.info("Creating interfaces...")
        for interface in interfaces:
            name = interface["name"]
            if "vlan" in interface:
                self.interfaces[name] = TestInterface(interface["iface"],
                                                      vlan=interface["vlan"])
            else:
                self.interfaces[name] = TestInterface(interface["iface"])
        self.connected_nets = {}
        logger.info("Creating done")
        return

    def interface(self, name):
        return self.interfaces[name]

    def add_network(self, net, ifaces):
        if type(ifaces) != list:
            ifaces = [ifaces]
        for iface in ifaces:
            if iface not in self.interfaces:
                err_msg = "Interface '{}' not found on tester".format(iface)
                raise ValueError(err_msg)
        if net not in self.connected_nets:
            self.connected_nets[net] = ifaces
        else:
            self.connected_nets[net].extend(ifaces)
        return

    def get_net_iface(self, net):
        return self.get_net_ifaces(net)[0]

    def get_net_ifaces(self, net):
        try:
            return [self.interfaces[iface]
                    for iface in self.connected_nets[net]]
        except KeyError:
            err_msg = "Tester has no interfaces in net '{}'".format(net)
            raise KeyError(err_msg)

    def cleanup(self):
        logger.info("Cleaning...")
        for iface in self.interfaces.itervalues():
            iface.delete_subiface()
        logger.info("Cleaning done")

    def set_service_enabled(self, service, enable):
        if service == "tftp":
            proto_and_port = "-p udp --dport {}".format(self.tftp_port)
        elif service == "http":
            proto_and_port = "-p tcp --dport {}".format(self.tftp_port)
        else:
            raise ValueError("Unknown service: '{}'".format(service))
        cmd = "{} {{}} INPUT {} -j DROP".format(self.iptables, proto_and_port)
        with open(os.devnull, "a") as devnull:
            logger.debug("Execute: '{}'".format(cmd))
            check_cmd = cmd.format("-C").split()
            allowed = subprocess.call(check_cmd, stdout=devnull, stderr=devnull)
            if enable and not allowed:
                en_cmd = cmd.format("-D").split()
                logger.debug("Execute: '{}'".format(en_cmd))
                subprocess.check_call(en_cmd, stdout=devnull, stderr=devnull)
            elif not enable and allowed:
                dis_cmd = cmd.format("-A").split()
                logger.debug("Execute: '{}'".format(dis_cmd))
                subprocess.check_call(dis_cmd, stdout=devnull, stderr=devnull)
            else:
                pass  # nothing to do
        return
