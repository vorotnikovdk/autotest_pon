#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
import os
import json
import scriptik
import testfile
import logging

logger = logging.getLogger("tester.device")


class Device(object):
    """ Wrapper for scriptik.Device that adds testing-related logic"""

    def __init__(self, device_config, tester):
        self.device = scriptik.create_device(device_config)
        self.tester = tester
        main_hw = device_config["hardware"].replace(":", "_")
        main_sw = device_config["software"]
        alt_hw = device_config.get("alt_hardware", main_hw).replace(":", "_")
        alt_sw = device_config.get("alt_software", main_sw)
        self.dev_path = os.path.join(self.tester.files, main_hw, main_sw)
        self.alt_dev_path = os.path.join(self.tester.files, alt_hw, alt_sw)
        self.url_fmt = testfile.create_url_formatter(self.tester, self.device)
        self.connected_nets = {}
        logger.info("Created device main: |{}|{}| "
                    "alt: |{}|{}|')".format(main_hw, main_sw, alt_hw, alt_sw))

    def get_file(self, file_path):
        """
        Builds path to target file using `Device's` hardware, software and
        passed `file_path`.
        If file could not be found - tries to use alternative hardware and
        software.
        Returns `TestFile` instance or raises IOError.

        :param file_path: path to file
        :returns: `TestFile` instance
        """
        try:
            tf = testfile.TestFile(os.path.join(self.dev_path, file_path),
                                   self.url_fmt)
            logger.debug("Got TestFile '{}'".format(tf.path))
            return tf
        except IOError:
            pass
        logger.warning("Not found '{}' in main dir {}".format(file_path,
                                                              self.dev_path))
        try:
            tf = testfile.TestFile(os.path.join(self.alt_dev_path, file_path),
                                   self.url_fmt)
            logger.debug("Got TestFile {}".format(tf.path))
            return tf
        except IOError:
            err_msg = "Not found '{}' in main dir '{}' and alt dir " \
                      "'{}'".format(file_path, self.dev_path, self.alt_dev_path)
            logger.error(err_msg)
            raise IOError(err_msg)

    def get_dir(self, dir_path):
        """
        Builds path to target dir using `Device's` hardware, software and
        passed `dir_path`.
        If dir could not be found - tries to use alternative hardware and
        software.
        Returns `TestDir` instance or raises IOError.

        :param dir_path: path to dir
        :returns: `TestDir` instance
        """
        path = os.path.join(self.dev_path, dir_path)
        if os.path.isdir(path):
            td = testfile.TestDir(path, self.url_fmt)
            logger.debug("Got TestDir {}".format(td.path))
            return td
        logger.warning("Not found '{}' in main dir {}".format(dir_path,
                                                              self.dev_path))
        path = os.path.join(self.alt_dev_path, dir_path)
        if os.path.isdir(path):
            td = testfile.TestDir(path, self.url_fmt)
            logger.debug("Got TestDir {}".format(td.path))
            return td
        err_msg = "Not found '{}' in main dir {} and alt dir " \
                  "{}".format(dir_path, self.dev_path, self.alt_dev_path)
        logger.error(err_msg)
        raise IOError(err_msg)

    def add_network(self, net, ifaces):
        if type(ifaces) != list:
            ifaces = [ifaces]
        if net not in self.connected_nets:
            self.connected_nets[net] = ifaces
        else:
            self.connected_nets[net].extend(ifaces)
        return

    def get_net_iface(self, net):
        return self.get_net_ifaces(net)[0]

    def get_net_ifaces(self, net):
        try:
            return self.connected_nets[net]
        except KeyError:
            err_msg = "Device '{}' has no interfaces in net '{}'".format(
                self.hardware, net)
            raise KeyError(err_msg)

    def run_command_from_file(self, command_file, timeout=120):
        """
        Reads all commands in specified `command_file` and calls
        CLIDevice.run_command() for each line. Requires `default_cli` class
        attribute for CLIDevice
        :param command_file: opened file or TestFile
        :param timeout: timeout in seconds for each command
        :return:
        """
        for line in command_file.readlines():
                self.device.run_command(line, timeout=timeout)
        return

    def __getattr__(self, item):
        return getattr(self.device, item)
