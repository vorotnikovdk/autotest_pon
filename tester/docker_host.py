#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from scapy.all import *
import re
import os
import time
import logging
import subprocess

logger = logging.getLogger("tester.docker_host")

class DockerHost(object):
    image = "eltexpon/dhcpv4d"

    def __init__(self, mac=None, net="none", volume=None,
                 privileged=False, args=""):
        logger.info("Starting Docker '{}'".format(self.image))
        cmd = "/usr/bin/docker run -it -d --cap-add net_admin"
        cmd += " --net={}".format(net)
        macaddr = str(RandMAC("00:*")) if mac is None else mac
        cmd += " --mac-address={}".format(macaddr)
        if privileged:
            cmd += " --privileged"
        if volume is not None:
            cmd += " -v \"{}\"".format(volume)
        cmd += " {}".format(self.image)
        if args:
            cmd += " {}".format(args)
        logger.debug("Execute: '{}'".format(cmd))
        result = subprocess.check_output(cmd, shell=True,
                                         stderr=subprocess.STDOUT)
        self.id = result.rstrip()
        self.mac = macaddr

    @property
    def alive(self):
        cmd = 'docker inspect --format="{{{{ .State.Running }}}}" ' \
              '{}'.format(self.id)
        try:
            logger.debug("Execute: '{}'".format(cmd))
            state = subprocess.check_output(cmd, shell=True,
                                            stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError:
            return False
        return state.rstrip() == "true"

    def execute(self, command):
        cmd = "/usr/bin/docker exec {} {}".format(self.id, command)
        logger.debug("Execute: '{}'".format(cmd))
        return subprocess.check_output(cmd, shell=True,
                                       stderr=subprocess.STDOUT)

    def ping(self, dst_ip):
        command = "ping -c 1 -W 1 {}".format(dst_ip)
        try:
            logger.debug("Execute: '{}'".format(cmd))
            res = self.execute(command)
        except subprocess.CalledProcessError:
            return False
        match = re.search(r"(\d+)% packet loss", res)
        if not match:
            return False
        return int(match.group(1)) == 0

    def ip_addresses(self, ip_family=4, outer_vlan=0, inner_vlan=0):
        ifname = "eth0"
        if outer_vlan:
            ifname += ".{}".format(outer_vlan)
            if inner_vlan:
                ifname += ".{}".format(inner_vlan)
        cmd = "ls -1 /sys/class/net"
        interfaces = self.execute(cmd)
        if ifname not in interfaces.split():
            raise ValueError("Interface {} not exists".format(ifname))
        if ip_family not in [4, 6]:
            raise ValueError("Incorrect IP protocol")
        addr_type = "inet" if ip_family == 4 else "inet6"
        cmd = "ip -f {} addr show dev {} | grep inet | " \
              "cut -d' ' -f6".format(addr_type, ifname)
        output = self.execute(cmd)
        return output.split()

    def ip_ready(self, ip_family=4, outer_vlan=0, inner_vlan=0, timeout=10):
        stop_time = time.time() + timeout
        while time.time() <= stop_time:
            ip_address = self.ip_addresses(ip_family=ip_family,
                                           outer_vlan=outer_vlan,
                                           inner_vlan=inner_vlan)
            if ip_address:
                break
            else:
                time.sleep(2.5)
        else:
            return False
        return True

    def stop(self, force=False):
        if force:
            cmd = "/usr/bin/docker kill {}".format(self.id)
        else:
            cmd = "/usr/bin/docker stop -t3 {}".format(self.id)
        with open(os.devnull) as null:
            subprocess.call(cmd, shell=True, stdout=null, stderr=null)
        return


class DHCPv4Client(DockerHost):
    image = "eltexpon/dhcpv4c"


class DHCPv4Server(DockerHost):
    image = "eltexpon/dhcpv4s"


class PPPoEClient(DockerHost):
    image = "eltexpon/pppoec"

    def ip_addresses(self, ip_family=4, outer_vlan=0, inner_vlan=0):
        # FixMe: only ppp0 supported
        logger.warning("PPPoEClient only supports 'ppp0' interface")
        ifname = "ppp0"
        cmd = "ls -1 /sys/class/net"
        interfaces = self.execute(cmd)
        if ifname not in interfaces.split():
            return []
        addr_type = "inet"
        cmd = "ip -f {} addr show dev {} | grep inet | " \
              "cut -d' ' -f6".format(addr_type, ifname)
        output = self.execute(cmd)
        return output.split()

    def stop(self, force=False):
        pppd = self.execute("pidof pppd")
        self.execute("kill -1 {}".format(pppd))
        super(PPPoEClient, self).stop(force=force)


class PPPoEServer(DockerHost):
    image = "eltexpon/pppoes"

    def ip_addresses(self, ip_family=4, outer_vlan=0, inner_vlan=0):
        # FixMe: not supported
        logger.error("PPPoEServer doesnt supported")
        raise NotImplementedError("ppp server")


class StaticIPv4Client(DockerHost):
    image = "eltexpon/staticip4"
