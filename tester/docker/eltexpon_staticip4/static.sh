#!/bin/sh
outervid=$1
innervid=$2
src=$3

iface="eth0"
ip address flush dev $iface > /dev/null 2>&1
ip link set dev $iface up > /dev/null 2>&1
if [[ $outervid -ne 0 ]]; then
	ip link add link $iface name $iface.$outervid type vlan id $outervid > /dev/null 2>&1
	iface="eth0.$outervid"
	ip link set dev $iface up > /dev/null 2>&1
	if [[ $innervid -ne 0 ]]; then
		ip link add link $iface name $iface.$innervid type vlan id $innervid > /dev/null 2>&1
		iface="eth0.$outervid.$innervid"
		ip link set dev $iface up > /dev/null 2>&1
	fi
fi
ip address add $src dev $iface > /dev/null 2>&1
sh
