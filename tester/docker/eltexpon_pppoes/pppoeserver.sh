#!/bin/bash
iface="eth0"
ip address flush dev $iface > /dev/null 2>&1
ip link set dev $iface up > /dev/null 2>&1

for var in "$@"
do
	iface="eth0"
	read -a interface <<< $var
	outervid=${interface[0]}
	innervid=${interface[1]}
	if [[ $outervid -ne 0 ]]; then
		ip link add link $iface name $iface.$outervid type vlan id $outervid
		iface="eth0.$outervid"
		ip link set dev $iface up 
		if [[ $innervid -ne 0 ]]; then
			ip link add link $iface name $iface.$innervid type vlan id $innervid
			iface="eth0.$outervid.$innervid"
			ip link set dev $iface up
		fi
	fi
done
/usr/local/sbin/accel-pppd -c /etc/accel.conf & > /dev/null 2>&1
bash