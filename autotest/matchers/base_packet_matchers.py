#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from scapy.all import *
from hamcrest.core.base_matcher import BaseMatcher
from hamcrest.core.helpers.hasmethod import hasmethod

load_contrib("ospf")


class ScapyPacketMatcher(BaseMatcher):
    """
    Parent class for basic packet matchers.
    Used to match layers and it's fields values.
    For complicate protocols and fields (ex. DHCP options) use protocol specific
    matchers.
    Returns scapy's packet representation instead of passed packet.
    """

    def __init__(self, layer_name, number=1, field=None, value=None,
                 negate_result=False):
        super(ScapyPacketMatcher, self).__init__()
        self.layer_name = layer_name
        self.number = number
        self.field = field
        self.value = value
        self.negate_result = negate_result

    def get_layer(self, item):
        return item.getlayer(self.layer_name, nb=self.number)

    def describe_mismatch(self, item, mismatch_description):
        mismatch_description.append_text('was ')
        mismatch_description.append_description_of(repr(item))


class HasLayer(ScapyPacketMatcher):
    def _matches(self, item):
        if not hasmethod(item, "getlayer"):
            return False
        return bool(self.get_layer(item)) ^ self.negate_result

    def describe_to(self, description):
        match = "has" if not self.negate_result else "hasn't"
        desc = "Packet {} layer '{}'".format(match, self.layer_name)
        description.append_text(desc)


def contains_layer(layer, number=1):
    return HasLayer(layer, number=number)


def contains_no_layer(layer, number=1):
    return HasLayer(layer, number=number, negate_result=True)


class FieldIsEqualTo(ScapyPacketMatcher):
    def _matches(self, item):
        if not hasmethod(item, "getlayer"):
            return False
        item_layer = self.get_layer(item)
        item_value = getattr(item_layer, self.field, None)
        if item_value is not None:
            return (item_value == self.value) ^ self.negate_result
        return False

    def describe_to(self, description):
        match = "==" if not self.negate_result else "!="
        desc = "Packet has '{}_{}.{}' {} '{}'".format(self.layer_name,
                                                      self.number,
                                                      self.field,
                                                      match,
                                                      self.value)
        description.append_text(desc)


def contains_field(layer, field, value, layer_number=1):
    return FieldIsEqualTo(layer, number=layer_number, field=field, value=value)


def contains_no_field(layer, field, value, layer_number=1):
    return FieldIsEqualTo(layer, number=layer_number, field=field, value=value,
                          negate_result=True)
