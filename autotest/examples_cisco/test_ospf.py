#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
import time
from hamcrest import *
from matchers import *
from utils import *
import pytest


@pytest.allure.feature("Hello Protocol")
@pytest.mark.usefixtures("unl_lab")
@pytest.mark.parametrize("unl_lab",
                         ["brtest.unl"],
                         ids=[""],
                         indirect=True,
                         scope="class")
class TestOSPFHelloProtocol:
    @pytest.allure.story("Базовая проверка Hello протокола")
    @pytest.mark.parametrize("hello_interval",
                             [10, 25],
                             ids=[
                                 "hello_interval=10",
                                 "hello_interval=25"
                             ])
    def test_basic_verification(self, hello_interval):
        net10 = "10.10.10.0/24"
        router_port = self.dut.get_net_iface(net10)
        tester_port = self.tester.get_net_iface(net10)
        with self.dut.access():
            self.dut.set_interface_ospf_timers(router_port,
                                               hello=hello_interval,
                                               dead=hello_interval * 4)
        with pytest.allure.step("Hello interval равен "
                                "{} секунд".format(hello_interval)):
            tester_port.start_sniffer(timeout=hello_interval * 2 + 1,
                                      bpf_filter="proto ospf")
            tester_port.wait_for_sniffer()
            assert_that(tester_port.sniffed_pkts,
                        any_of(has_length(2), has_length(3)))
            for packet in tester_port.sniffed_pkts:
                assert_that(packet,
                            all_of(contains_layer("OSPF_Hello"),
                                   contains_field("IP", "dst", "224.0.0.5")))
            intervals = get_pkt_list_intervals(tester_port.sniffed_pkts)
            avg = sum(intervals) / float(len(intervals))
            assert_that(avg,
                        is_(close_to(hello_interval, 0.1 * hello_interval)))
