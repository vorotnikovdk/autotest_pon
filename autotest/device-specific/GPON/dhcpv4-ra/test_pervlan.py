#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from hamcrest import *
from scapy.all import *
from pytest import allure, yield_fixture, mark
from autotest.utils.scapy_tools import parse_dhcpv4_opt82, printable_pkts
import logging

logger = logging.getLogger("gpon.dhcpv4_ra.opt82")


@yield_fixture(scope="module")
def dhcpv4_server(request, dut, test_server):
  logger.info("Fixture 'dhcpv4_server' start")
  up_port = test_server.interface("link1")
  dhcp_config = dut.get_file("dhcpv4-ra/dhcpd_per_vlan.conf")
  server = up_port.dhcpv4_server(dhcp_config.path, None,
                                 "10 0 10.31.0.1/24",
                                 "11 0 10.41.0.1/24",
                                 "200 200 10.32.0.1/24",
                                 "300 300 10.42.0.1/24"
                                 )
  logger.info("Fixture 'dhcpv4_server' done")
  yield server
  logger.info("Fixture 'dhcpv4_server' teardown")
  server.stop()
  logger.info("Fixture 'dhcpv4_server' finished")


@allure.feature("DHCPv4RA PerVlan")
@mark.usefixtures("datapath_model")
@mark.usefixtures("olt_configuration")
@mark.usefixtures("dhcpv4ra_config")
@mark.usefixtures("ont_configuration")
@mark.usefixtures("dhcpv4_server")
class TestPerVlan:
  parameters = [{"fixture": "datapath_model",
                 "in_hardware": "MA4000|LTP-4|LTP-8",
                 "indirect": True,
                 "params": [{"value": "model1.conf",
                             "test_id": "Model1"},
                            {"value": "model2.conf",
                             "test_id": "Model2"},
                            {"value": "model3.conf",
                             "test_id": "Model3"}],
                 "scope": "session"},
                {"fixture": "olt_configuration",
                 "indirect": True,
                 "params": [{"value": "dhcpv4-ra/per_vlan.conf"}],
                 "scope": "module"
                 },
                {"fixture": "ont_configuration",
                 "indirect": True,
                 "params": [{"value": "dhcpv4-ra/per_vlan_single_tag.conf",
                             "test_id": "SingleTag"},
                            {"value": "dhcpv4-ra/per_vlan_double_tag.conf",
                             "test_id": "DoubleTagged"},
                            ],
                 "scope": "class"
                 },
                {"fixture": "dhcpv4ra_config,expected_ncid,expected_mcid",
                 "indirect": ["dhcpv4ra_config"],
                 "params": [{"value": ("dhcpv4-ra/global_dis_vlan_dis.conf",
                                       "CID",
                                       "CID"),
                             "test_id": "AllDisabled",
                             "markers": [allure.story("Global OFF Vlan OFF")]
                             },
                            {"value": ("dhcpv4-ra/global_en_vlan_dis.conf",
                                       "global",
                                       "CID"),
                             "test_id": "OnlyGlobal",
                             "markers": [allure.story("Global ON Vlan OFF")]
                             },
                            {"value": ("dhcpv4-ra/global_dis_vlan_en.conf",
                                       "CID",
                                       "vlan"),
                             "test_id": "OnlyVlan",
                             "markers": [allure.story("Global OFF Vlan ON")]
                             },
                            {"value": ("dhcpv4-ra/global_en_vlan_en.conf",
                                       "global",
                                       "vlan"),
                             "test_id": "GlobalAndVlan",
                             "markers": [allure.story("Global ON Vlan ON")]
                             },
                            ]
                 }
                ]

  def test_profile_correct(self, expected_mcid, expected_ncid,
                           ont_configuration):
    logger.info("Test 'profile_correct' start")
    uplink = self.tester.interface("link1")
    with allure.step("Start uplink sniffer"):
      bpf = "(udp and src port 68) or  (vlan and udp and src port 68)"
      uplink.start_sniffer(bpf_filter=bpf)
    with allure.step("Start DHCP in matching and not matching vlans"):
      mclients = {}
      nclients = {}
      for ont in ont_configuration:
        port = self.tester.interface(ont)
        mclients[ont] = port.dhcpv4_client(outer_vlan=10,
                                           opt82="01034349440203524944")
        nclients[ont] = port.dhcpv4_client(outer_vlan=11,
                                           opt82="01034349440203524944")
    with allure.step("Get DHCP results and stop DHCP clients"):
      mresults = {}
      nresults = {}
      for ont, client in mclients.iteritems():
        mresults[ont] = client.ip_ready(outer_vlan=10)
        client.stop()
      for ont, client in nclients.iteritems():
        nresults[ont] = client.ip_ready(outer_vlan=11)
        client.stop()
    with allure.step("Check DHCP results"):
      uplink.stop_sniffer()
      for ont, result in mresults.iteritems():
        assert_that(result,
                    described_as("ONT {} client got lease "
                                 "for mac {}".format(ont, mclients[ont].mac),
                                 is_(True)))
      for ont, result in nresults.iteritems():
        assert_that(result,
                    described_as("ONT {} client got lease "
                                 "for mac  {}".format(ont, nclients[ont].mac),
                                 is_(True)))
    with allure.step("Check if all applied VLAN profile is correct"):
      for ont, client in mclients.iteritems():
        pkts = filter(lambda p: p.src == client.mac, uplink.sniffed_pkts)
        allure.attach("{} VLAN packets".format(ont), printable_pkts(pkts))
        assert_that(pkts,
                    described_as("Ont {} VLAN pkts sniffed".format(ont),
                                 has_length(greater_than_or_equal_to(2))))
        for pkt in pkts:
          cid, rid = parse_dhcpv4_opt82(pkt)
          cid_desc = "Circuit-id for ont {} and " \
                     "mac {} was {}".format(ont, client.mac, expected_mcid)
          assert_that(cid,
                      described_as(cid_desc,
                                   is_(equal_to(expected_mcid))))
    with allure.step("Check if all applied global profile is correct"):
      for ont, client in nclients.iteritems():
        pkts = filter(lambda p: p.src == client.mac, uplink.sniffed_pkts)
        allure.attach("{} global packets".format(ont), printable_pkts(pkts))
        assert_that(pkts,
                    described_as("Ont {} global pkts sniffed".format(ont),
                                 has_length(greater_than_or_equal_to(2))))
        for pkt in pkts:
          cid, rid = parse_dhcpv4_opt82(pkt)
          cid_desc = "Circuit-id for ont {} and " \
                     "mac {} was {}".format(ont, client.mac, expected_ncid)
          assert_that(cid,
                      described_as(cid_desc,
                                   is_(equal_to(expected_ncid))))
    logger.info("Test 'profile_correct' done")