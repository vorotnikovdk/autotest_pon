#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from hamcrest import *
from scapy.all import *
from pytest import allure, mark, yield_fixture
from autotest.utils.scapy_tools import parse_dhcpv4_opt82, printable_pkts
import time
import logging

logger = logging.getLogger("gpon.dhcpv4_ra.opt82")


@yield_fixture(scope="module")
def dhcpv4_server(request, dut, test_server):
  logger.info("Fixture 'dhcpv4_server' start")
  up_port = test_server.interface("link1")
  dhcp_config = dut.get_file("dhcpv4-ra/dhcpd.conf")
  server = up_port.dhcpv4_server(dhcp_config.path, None,
                                 "10 0 10.31.0.1/24",
                                 "200 200 10.32.0.1/24"
                                 )
  logger.info("Fixture 'dhcpv4_server' done")
  yield server
  logger.info("Fixture 'dhcpv4_server' teardown")
  server.stop()
  logger.info("Fixture 'dhcpv4_server' finished")


# Option82 CID:RID == "01034349440203524944"
@allure.feature("DHCPv4RA Option82")
@mark.usefixtures("datapath_model")
@mark.usefixtures("olt_configuration")
@mark.usefixtures("dhcpv4ra_config")
@mark.usefixtures("ont_configuration")
@mark.usefixtures("dhcpv4_server")
class TestOption82:
  parameters = [{"fixture": "datapath_model",
                 "in_hardware": "MA4000|LTP-4|LTP-8",
                 "indirect": True,
                 "params": [{"value": "model1.conf",
                             "test_id": "Model1"},
                            {"value": "model2.conf",
                             "test_id": "Model2"},
                            {"value": "model3.conf",
                             "test_id": "Model3"}],
                 "scope": "session"},
                {"fixture": "olt_configuration",
                 "indirect": True,
                 "params": [{"value": "dhcpv4-ra/option82.conf"}],
                 "scope": "module"
                 },
                {"fixture": "ont_configuration",
                 "indirect": True,
                 "params": [{"value": "dhcpv4-ra/opt82_single_tag.conf",
                             "test_id": "SingleTag"},
                            {"value": "dhcpv4-ra/opt82_double_tag.conf",
                             "test_id": "DoubleTagged"},
                            ],
                 "scope": "class"
                 },
                {"fixture": "dhcpv4ra_config,cid,rid,opt82",
                 "indirect": ["dhcpv4ra_config"],
                 "params": [{"value": ("dhcpv4-ra/disabled.conf",
                                       None,
                                       None,
                                       None),
                             "test_id": "DisabledInsert",
                             "markers": [allure.story("Disabled Agent - No Option")]
                             },
                            {"value": ("dhcpv4-ra/disabled.conf",
                                       "CID",
                                       "RID",
                                       "01034349440203524944"),
                             "test_id": "DisabledReplace",
                             "markers": [allure.story("Disabled Agent - Pass Option")],
                             },
                            {"value": ("dhcpv4-ra/no_over.conf",
                                       None,
                                       None,
                                       None),
                             "test_id": "NoOverwriteInsert",
                             "markers": [allure.story("No Overwrite - No Option"),
                                         allure.issue("http://red.eltex.loc/issues/69994")]
                             },
                            {"value": ("dhcpv4-ra/no_over.conf",
                                       "CID",
                                       "RID",
                                       "01034349440203524944"),
                             "test_id": "NoOverwriteReplace",
                             "markers": [allure.story("No Overwrite - Pass Option")],
                             },
                            {"value": ("dhcpv4-ra/ont_info.conf",
                                       "dhcptest-%MNGIP%-%GPON-PORT%-%ONTID%-%PONSERIAL%-%GEMID%-%VLAN0%-%VLAN1%-%MAC%-",
                                       "dhcptest-%MNGIP%-%GPON-PORT%-%ONTID%-%PONSERIAL%-%GEMID%-%VLAN0%-%VLAN1%-%MAC%-",
                                       None),
                             "test_id": "InfoInsert",
                             "markers": [allure.story("ONT Info - Insert Option")],
                             },
                            {"value": ("dhcpv4-ra/ont_info.conf",
                                       "dhcptest-%MNGIP%-%GPON-PORT%-%ONTID%-%PONSERIAL%-%GEMID%-%VLAN0%-%VLAN1%-%MAC%-",
                                       "dhcptest-%MNGIP%-%GPON-PORT%-%ONTID%-%PONSERIAL%-%GEMID%-%VLAN0%-%VLAN1%-%MAC%-",
                                       "01034349440203524944"),
                             "test_id": "InfoReplace",
                             "markers": [allure.story("ONT Info - Replace Option")],
                             },
                            {"value": ("dhcpv4-ra/opt_60_82.conf",
                                       "udhcp 1.24.2--",
                                       "udhcp 1.24.2--",
                                       None),
                             "test_id": "Opt60And82Insert",
                             "markers": [allure.story("Opt60And82 - Insert Option")],
                             },
                            {"value": ("dhcpv4-ra/opt_60_82.conf",
                                       "udhcp 1.24.2--",
                                       "udhcp 1.24.2--",
                                       None),
                             "test_id": "Opt60And82Replace",
                             "markers": [allure.story("Opt60And82 - Replace Option")],
                             },
                            ]
                 }
                ]

  def test_option82(self, dhcpv4ra_config, cid, rid, opt82, ont_configuration):
    logger.info("Test 'option82' start")
    uplink = self.tester.interface("link1")
    with allure.step("Start uplink sniffer"):
      bpf = "(udp and src port 68) or (vlan and udp and src port 68)"
      uplink.start_sniffer(bpf_filter=bpf)
    with allure.step("Start DHCP clients"):
      clients = {}
      for ont in ont_configuration:
        port = self.tester.interface(ont)
        clients[ont] = port.dhcpv4_client(outer_vlan=10, opt82=opt82)
    with allure.step("Get DHCP results and stop DHCP clients"):
      results = {}
      for ont, client in clients.iteritems():
        results[ont] = client.ip_ready(outer_vlan=10)
        client.stop()
    with allure.step("Check DHCP results"):
      uplink.stop_sniffer()
      for ont, result in results.iteritems():
        assert_that(result,
                    described_as("ONT {} client got lease "
                                 "for mac {}".format(ont, clients[ont].mac),
                                 is_(True)))
    with allure.step("Check if all packets has correct options"):
      for ont, client in clients.iteritems():
        macs = self.dut.get_ont_mac_table(ont_configuration[ont])
        mac_record = filter(lambda x: x['mac'] == client.mac, macs)[0]
        ok_cid = ok_rid = None
        if cid is not None:
          ok_cid = self.dut.fill_interface_id(cid, ont, mac_record)
        if rid is not None:
          ok_rid = self.dut.fill_interface_id(rid, ont, mac_record)
        pkts = filter(lambda p: p.src == client.mac, uplink.sniffed_pkts)
        allure.attach("{} DHCP packets".format(ont), printable_pkts(pkts))
        assert_that(pkts,
                    described_as("ONT {} DHCP pkts sniffed".format(ont),
                                 has_length(greater_than_or_equal_to(2))))
        for pkt in pkts:
          p_cid, p_rid = parse_dhcpv4_opt82(pkt)
          cid_desc = "Circuit-id for ont {} and " \
                     "mac {} was {}".format(ont,client.mac, ok_cid)
          assert_that(p_cid,
                      described_as(cid_desc,
                                   is_(equal_to(ok_cid))))
          rid_desc = "Remote-id for ont {} and " \
                     "mac {} was {}".format(ont, client.mac, ok_rid)
          assert_that(p_rid,
                      described_as(rid_desc,
                                   is_(equal_to(ok_rid))))
    logger.info("Test 'option82' done")
