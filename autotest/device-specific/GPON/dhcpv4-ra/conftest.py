#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from pytest import yield_fixture
import logging

logger = logging.getLogger("gpon.dhcpv4_ra.conftest")


@yield_fixture(scope="class")
def dhcpv4ra_config(request, dut, olt_configuration):
    logger.info("Fixture 'dhcpv4ra_config' start")
    config = dut.get_file(request.param)
    dut.run_commands_from(config, timeout=120)
    logger.info("Fixture 'dhcpv4ra_config' done")
    yield
