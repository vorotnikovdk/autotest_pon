#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from pytest import yield_fixture
import os
import logging

logger = logging.getLogger("gpon.conftest")


@yield_fixture(scope="module")
def datapath_model(request, dut):
  logger.info("Fixture 'datapath_model' start")
  if hasattr(request, "param"):
    config = dut.get_file(request.param)
    dut.run_commands_from(config, timeout=150)
    logger.info("Fixture 'datapath_model' done")
    yield request.param
  else:
    logger.info("Fixture 'datapath_model' cancelled")
    yield None


@yield_fixture(scope="module")
def olt_configuration(request, dut, datapath_model):
  logger.info("Fixture 'olt_configuration' start")
  if hasattr(request, "param"):
    config = dut.get_file(request.param)
    dut.run_commands_from(config, timeout=150)
    dut.olt_ready()
    logger.info("Fixture 'olt_configuration' done")
    yield request.param
    logger.info("Fixture 'olt_configuration' teardown")
    if os.path.isfile("{}_undo".format(config.path)):
      undo_file = dut.get_file("{}_undo".format(request.param))
      dut.run_commands_from(undo_file, timeout=150)
    logger.info("Fixture 'olt_configuration' finished")
  else:
    logger.info("Fixture 'olt_configuration' cancelled")
    yield None


@yield_fixture(scope="module")
def ont_configuration(request, dut, olt_configuration):
  logger.info("Fixture 'ont_configuration' start")
  if hasattr(request, "param"):
    config = dut.get_file(request.param)
    ont_data = {}
    for ont, sn in dut.onts.iteritems():
      ont_data["ont_{}_sn".format(ont)] = sn
    config.fill(**ont_data)
    dut.run_commands_from(config, timeout=120)
    ready = dut.onts_ready()
    logger.info("Fixture 'ont_configuration' done")
    yield ready
  else:
    logger.info("Fixture 'ont_configuration' cancelled")
    yield None
