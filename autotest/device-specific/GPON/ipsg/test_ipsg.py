#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from hamcrest import *
from scapy.all import *
from pytest import allure, mark
from autotest.utils.scapy_tools import printable_pkts
import logging

@yield_fixture(scope="module")
def dhcpv4_server(request, dut, test_server):
    print("DHCP", time.time())
    up_port = test_server.interface("link1")
    dhcp_config = dut.get_file("ipsg/dhcpd.conf")
    server1 = up_port.dhcpv4_server(dhcp_config.path, None, "10 0 10.0.0.1/11")
    server2 = up_port.dhcpv4_server(dhcp_config.path, None,
                                    "200 200 10.0.0.1/11")
    print("DHCP_DONE", time.time())
    yield server1, server2
    server1.stop()
    server2.stop()


@yield_fixture(scope="class")
def ipsg_config(request, dut, olt_configuration):
    print("IPSG", time.time())
    with dut.access():
        config = dut.get_file(request.param)
        dut.run_commands_from(config, timeout=120)
        dut.olt_ready()
    print("IPSG_DONE", time.time())
    yield

pytestmark = [
    mark.parametrize("datapath_model",
                            ["model2.conf", "model3.conf"],
                            ids=["Model2", "Model3"],
                            indirect=True, scope="session"),
    mark.parametrize("olt_configuration",
                            ["ipsg/ipsg.conf"],
                            ids=[""],
                            indirect=True, scope="module"),
]





# Option82 CID:RID == "01034349440203524944"
@allure.feature("IP Source Guard")
@mark.usefixtures("datapath_model")
@mark.usefixtures("olt_configuration")
@mark.usefixtures("ont_configuration")
@mark.usefixtures("ipsg_config")
@mark.usefixtures("dhcpv4_server")
@mark.parametrize("ont_configuration",
                  ["ipsg/single_tag.conf",
                   "ipsg/double_tag.conf"],
                   ids=["SingleTagged", "DoubleTagged"],
                   indirect=True, scope="class")
@mark.parametrize("ipsg_config,enabled,dynamic,binds",
                      [("ipsg/disabled.conf", False, True, False),
                       ("ipsg/enable_disable.conf", False, True, False),
                       ("ipsg/dyn_nobind.conf", True, True, False),
                       ("ipsg/st_nobind.conf", True, False, False),
                       ("ipsg/st_bind.conf", True, False, True),
                       ],
                      ids=["Disabled", "EnableDisable", "DynamicNoBind",
                           "StaticNoBind", "StaticBind"],
                      indirect=["ipsg_config"], scope="class"
)
class TestIPSG:
    def test_dhcp_ping(self, ipsg_config, enabled, dynamic, binds,
                       ont_configuration):
        print("TEST1", time.time())
        expected_result = (not enabled or dynamic)
        uplink = self.tester.interface("link1")
        with allure.step("Start uplink sniffer"):
            bpf = "(icmp or (udp and src port 68)) or " \
                  "(vlan and (icmp or (udp and src port 68)))"
            uplink.start_sniffer(bpf_filter=bpf)
            print("SNIFFER", time.time())
        with allure.step("Start DHCP clients"):
            clients = {}
            for ont in ont_configuration:
                port = self.tester.interface(ont)
                clients[ont] = port.dhcpv4_client(outer_vlan=10)
        with allure.step("Send pings"):
            results = {}
            for ont, client in clients.iteritems():
                ip_ok = client.ip_ready(outer_vlan=10)
                ping_result = client.ping("10.0.0.1")
                client.stop()
                results[ont] = ip_ok, ping_result
        with allure.step("Check ping results"):
            uplink.stop_sniffer()
            print("SNIFFER_DONE", time.time())
            for ont, result in results.iteritems():
                assert_that(result[0],
                        described_as("ONT {} client got lease "
                                     "for mac {}".format(ont, clients[ont].mac),
                                     is_not(empty())))
                assert_that(result[1],
                        described_as("Ping for ONT {} and mac {} was "
                                     "successfull".format(ont,
                                                          clients[ont].mac),
                                     is_(expected_result)))
        with allure.step("Check if we have pkts on uplink"):
            for ont, client in clients.iteritems():
                pkts = filter(lambda p: p.src == client.mac,
                              uplink.sniffed_pkts)
                allure.attach("{} packets".format(ont), printable_pkts(pkts))
                icmp_passed = False
                for pkt in pkts:
                    if pkt.haslayer("ICMP"):
                        icmp_passed = True
                        break
                assert_that(icmp_passed,
                            described_as("ICMP packets ont {} mac {} "
                                         "passed".format(ont, client.mac),
                                         is_(expected_result)))
        print("TEST1_DONE", time.time())

    def test_ping_match_mac_ip(self, ipsg_config, enabled, dynamic, binds,
                               ont_configuration):
        print("TEST2", time.time())
        expected_result = (not enabled or binds)
        uplink = self.tester.interface("link1")
        with allure.step("Start uplink sniffer"):
            bpf = "icmp or (vlan and icmp)"
            uplink.start_sniffer(bpf_filter=bpf)
            print("SNIFFER", time.time())
        with allure.step("Start static IPv4 clients"):
            clients = {}
            for ont in ont_configuration:
                port = self.tester.interface(ont)
                slot, gport, onuid = ont.split('_')
                src_ip = "10.{}.{}.{}/11".format(int(slot),
                                                 int(gport),
                                                 int(slot) + 100)
                o4 = "{:02x}".format(int(slot))
                o5 = "{:02x}".format(int(gport))
                o6 = "{:02x}".format(int(onuid))
                smac = "00:22:33:{}:{}:{}".format(o4, o5, o6)
                client = port.ipv4_client(src_ip, outer_vlan=10, mac=smac)
                clients[ont] = client
        with allure.step("Send pings with matching IP and MAC"):
            results = {}
            for ont, client in clients.iteritems():
                client.ip_ready(outer_vlan=10)
                results[ont] = client.ping("10.0.0.1")
                client.stop()
        with allure.step("Check ping results"):
            uplink.stop_sniffer()
            for ont, result in results.iteritems():
                assert_that(result,
                        described_as("Ping for ONT {} and mac {} was "
                                     "successfull".format(ont,
                                                          clients[ont].mac),
                                     is_(expected_result)))
        with allure.step("Check if we have pkts on uplink"):
            print("SNIFFER_DONE", time.time())
            for ont, client in clients.iteritems():
                pkts = filter(lambda p: p.src == client.mac,
                              uplink.sniffed_pkts)
                allure.attach("{} packets".format(ont), printable_pkts(pkts))
                assert_that(bool(pkts),
                            described_as("ICMP packets ont {} mac {} "
                                         "passed".format(ont, client.mac),
                                         is_(expected_result)))
        print("TEST2_DONE", time.time())

    def test_ping_nomatch_mac_ip(self, ipsg_config, enabled, dynamic, binds,
                                 ont_configuration):
        print("TEST2", time.time())
        expected_result = not enabled
        uplink = self.tester.interface("link1")
        with allure.step("Start uplink sniffer"):
            bpf = "icmp or (vlan and icmp)"
            uplink.start_sniffer(bpf_filter=bpf)
            print("SNIFFER", time.time())
        with allure.step("Start static IPv4 clients"):
            clients = {}
            for ont in ont_configuration:
                port = self.tester.interface(ont)
                slot, gport, onuid = ont.split('_')
                src_ip = "10.{}.{}.{}/11".format(int(slot),
                                                 int(gport),
                                                 int(slot) + 100)
                o4 = "{:02x}".format(int(slot))
                o5 = "{:02x}".format(int(gport))
                o6 = "{:02x}".format(int(onuid))
                smac = "00:88:99:{}:{}:{}".format(o4, o5, o6)
                client = port.ipv4_client(src_ip, outer_vlan=10, mac=smac)
                clients[ont] = client
        with allure.step("Send pings with matching IP and MAC"):
            results = {}
            for ont, client in clients.iteritems():
                client.ip_ready(outer_vlan=10)
                results[ont] = client.ping("10.0.0.1")
                client.stop()
        with allure.step("Check ping results"):
            uplink.stop_sniffer()
            for ont, result in results.iteritems():
                assert_that(result,
                        described_as("Ping for ONT {} and mac {} was "
                                     "successfull".format(ont,
                                                          clients[ont].mac),
                                     is_(expected_result)))
        with allure.step("Check if we have pkts on uplink"):
            print("SNIFFER_DONE", time.time())
            for ont, client in clients.iteritems():
                pkts = filter(lambda p: p.src == client.mac,
                              uplink.sniffed_pkts)
                allure.attach("{} packets".format(ont), printable_pkts(pkts))
                assert_that(bool(pkts),
                            described_as("ICMP packets ont {} mac {} "
                                         "passed".format(ont, client.mac),
                                         is_(expected_result)))
        print("TEST2_DONE", time.time())
