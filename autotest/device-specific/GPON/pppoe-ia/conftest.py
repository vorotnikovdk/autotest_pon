#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from pytest import yield_fixture, allure
import time
import logging

logger = logging.getLogger("gpon.pppoe-ia.conftest")


@yield_fixture(scope="module")
def pppoe_server(request, dut, test_server):
    logger.info("Fixture 'pppoe_server' start")
    up_port = test_server.interface("link1")
    server = up_port.pppoe_server(None, "10 0", "200 200")
    logger.info("Fixture 'pppoe_server' done")
    yield
    logger.info("Fixture 'pppoe_server' teardown")
    server.stop()
    logger.info("Fixture 'pppoe_server' finished")


@yield_fixture()
def pppoeia_config(request, dut, ont_configuration):
    logger.info("Fixture 'pppoeia_config' start")
    config = dut.get_file(request.param)
    dut.run_commands_from(config, timeout=120)
    logger.info("Fixture 'pppoeia_config' done")
    yield


