#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from hamcrest import *
from scapy.all import *
from pytest import allure, mark
from autotest.utils.scapy_tools import parse_pppoe_vendor_tag, printable_pkts
import time
import logging

logger = logging.getLogger("gpon.pppoe-ia.vendor_tag")


@allure.feature("PPPoEIA VendorTag")
@mark.usefixtures("datapath_model")
@mark.usefixtures("pppoeia_config")
@mark.usefixtures("ont_configuration")
@mark.usefixtures("olt_configuration")
@mark.usefixtures("pppoe_server")
class TestVendorTag:
  parameters = [{"fixture": "datapath_model",
                 "in_hardware": "MA4000|LTP-4|LTP-8",
                 "indirect": True,
                 "params": [{"value": "model1.conf",
                             "test_id": "Model1"},
                            {"value": "model2.conf",
                             "test_id": "Model2"},
                            {"value": "model3.conf",
                             "test_id": "Model3"}],
                 "scope": "session"},
                {"fixture": "olt_configuration",
                 "indirect": True,
                 "params": [{"value": "pppoe-ia/cidrid.conf"}],
                 "scope": "module"
                 },
                {"fixture": "ont_configuration",
                 "indirect": True,
                 "params": [{"value": "pppoe-ia/single_tag.conf",
                             "test_id": "SingleTag"},
                            {"value": "pppoe-ia/double_tag.conf",
                             "test_id": "DoubleTagged"},
                            ],
                 "scope": "class"
                 },
                {"fixture": "pppoeia_config,cid,rid",
                 "indirect": ["pppoeia_config"],
                 "params": [{"value": ("pppoe-ia/disabled.conf",
                                       None,
                                       None),
                             "test_id": "Disabled",
                             "markers": [allure.story("PPPoE-IA Disabled")]
                             },
                            {"value": ("pppoe-ia/olt_info.conf",
                                       "pppoeia-%MNGIP%",
                                       "pppoeia-%MNGIP%"),
                             "test_id": "OLTInfo",
                             "markers": [allure.story("OLT Info")],
                             },
                            {"value": ("pppoe-ia/ont_info.conf",
                                       "%GPON-PORT%-%ONTID%-%PONSERIAL%-%GEMID%-%VLAN0%-%VLAN1%-%MAC%",
                                       "%GPON-PORT%-%ONTID%-%PONSERIAL%-%GEMID%-%VLAN0%-%VLAN1%-%MAC%"),
                             "test_id": "ONTInfo",
                             "markers": [allure.story("ONT Info")]
                             },
                            ]
                 }
                ]

  def test_vendor_tag_insert(self, cid, rid, ont_configuration):

    logger.info("Test 'vendor_tag_insert' start")
    uplink = self.tester.interface("link1")
    with allure.step("Start uplink sniffer"):
      bpf = "pppoed or (vlan and pppoed)"
      uplink.start_sniffer(bpf_filter=bpf)
    with allure.step("Start PPPoE clients"):
      clients = {}
      for ont in ont_configuration:
        port = self.tester.interface(ont)
        clients[ont] = port.pppoe_client(outer_vlan=10)
    with allure.step("Get PPPoE results and stop PPPoE clients"):
      results = {}
      for ont, client in clients.iteritems():
        results[ont] = client.ip_ready(outer_vlan=10)
        client.stop()
    with allure.step("Check PPPoE results"):
      uplink.stop_sniffer()
      for ont, result in results.iteritems():
        assert_that(result,
                    described_as("ONT {} client started ppp "
                                 "for mac {}".format(ont, clients[ont].mac),
                                 is_(True)))
    with allure.step("Check if all packets has correct options"):
      for ont, client in clients.iteritems():
        macs = self.dut.get_ont_mac_table(ont_configuration[ont])
        mac_record = filter(lambda x: x['mac'] == client.mac,
                            macs)[0]
        ok_cid = ok_rid = None
        if cid is not None:
          ok_cid = self.dut.fill_interface_id(cid, ont,
                                              mac_record)
        if rid is not None:
          ok_rid = self.dut.fill_interface_id(rid, ont,
                                              mac_record)
        pkts = filter(lambda p: p.src == client.mac,
                      uplink.sniffed_pkts)
        allure.attach("{} PPPoED packets".format(ont),
                      printable_pkts(pkts))
        assert_that(pkts,
                    described_as(
                      "ONT {} PPPoE pkts sniffed".format(ont),
                      has_length(greater_than_or_equal_to(2))))
        for pkt in pkts:
          vendor, p_cid, p_rid = parse_pppoe_vendor_tag(pkt)
          cid_desc = "Circuit-id for ont {} and mac {} " \
                     "was {}".format(ont, client.mac, ok_cid)
          assert_that(p_cid,
                      described_as(cid_desc,
                                   is_(equal_to(ok_cid))))
          rid_desc = "Remote-id for ont {} and mac {} " \
                     "was {}".format(ont, client.mac, ok_rid)
          assert_that(p_rid,
                      described_as(rid_desc,
                                   is_(equal_to(ok_rid))))
    logger.info("Test 'vendor_tag_insert' done")
