#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals


def user_action(message="Press Enter to continue..."):
    raw_input("\n{}".format(message))
    return


def user_agree(head="", body="", prompt="Is it ok?"):
    print("\n")
    if head:
        print("{}".format(head))
    if body:
        print("=" * 80)
        print(body)
        print("=" * 80)
    prompt = "{} [y/n]: ".format(prompt)
    ok = "?"
    while ok not in "yYnN":
        ok = raw_input(prompt) or "?"
    if ok in "yY":
        return True
    else:
        return False
