#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
import time
import requests
import urlparse
import contextlib

UNL_RUNNING = 2
UNL_STOPPED = 0


class RESTAPI(object):
    def __init__(self, host, basepath="api/", https=False):
        schema = "http" if not https else "https"
        self.host = host
        self.baseurl = "{}://{}/{}".format(schema, host, basepath)
        self.cookies = None
        self.username = ""
        self.password = ""

    def login(self, username, password):
        raise NotImplementedError("login")

    def request(self, method, resource, data=None):
        url = self.baseurl + resource
        for retry in xrange(2):
            if retry > 0:
                self.login(self.username, self.password)
            response = requests.request(method,
                                        url,
                                        json=data,
                                        cookies=self.cookies)
            if not self.cookies or response.status_code not in [400, 401]:
                break
        # noinspection PyUnboundLocalVariable
        if response.status_code != 200:
            raise UNLApiException(response.status_code)
        return response


class UNLApiException(Exception):
    pass


class UNetLab(RESTAPI):
    def login(self, username, password):
        resource = "auth/login"
        data = {"username": username, "password": password}
        resp = self.request("POST", resource, data=data)
        self.username = username
        self.password = password
        self.cookies = resp.cookies
        return

    def logout(self):
        resource = "auth/logout"
        resp = self.request("GET", resource)
        return resp.json()

    def get_all_labs(self, folder):
        resource = "folders/{}".format(folder)
        resp = self.request("GET", resource)
        data = resp.json()["data"]
        return data

    def list_labs(self, folder=""):
        labs = []
        data = self.get_all_labs(folder)
        for lab in data["labs"]:
            labs.append(lab["path"].lstrip("/"))
        if "folders" in data:
            for folder in data["folders"]:
                if folder["name"] != "..":
                    labs.extend(self.list_labs(folder["path"]))
        return labs

    def get_lab_info(self, path):
        resource = "labs/{}".format(path)
        resp = self.request("GET", resource)
        data = resp.json()["data"]
        return data

    def get_lab(self, path):
        resource = "labs/{}".format(path)
        data = self.get_lab_info(path)
        return UnlLab(self, resource, data)


class UnlLab(object):
    def __init__(self, unetlab, path, info):
        self.api = unetlab
        self.path = path + "/"
        self.info = info

    def get_all_nodes(self):
        resource = self.path + "nodes"
        resp = self.api.request("GET", resource)
        data = resp.json()["data"]
        if not data:
            return {}
        return data

    def list_nodes(self):
        data = self.get_all_nodes()
        nodes = []
        if data:
            for node_id, node in data.iteritems():
                nodes.append((node_id, node["name"]))
        return nodes

    def get_node_info(self, node_id):
        resource = self.path + "nodes/{}".format(node_id)
        resp = self.api.request("GET", resource)
        data = resp.json()["data"]
        return data

    def get_node(self, node_id):
        resource = self.path + "nodes/{}".format(node_id)
        data = self.get_node_info(node_id)
        return UnlNode(self.api, self, node_id, resource, data)

    def __wait_for_nodes_status(self, status, timeout):
        stop_time = time.time() + timeout
        while time.time() < stop_time:
            nodes = self.get_all_nodes()
            if all([node["status"] == status for node in nodes.itervalues()]):
                break
            time.sleep(1)
        else:
            raise UNLApiException("Nodes was not started within timeout")
        return

    def start_all_nodes(self, timeout=300):
        resource = self.path + "nodes/start"
        self.api.request("GET", resource)
        self.__wait_for_nodes_status(UNL_RUNNING, timeout)
        return

    def stop_all_nodes(self, timeout=300):
        resource = self.path + "nodes/stop"
        self.api.request("GET", resource)
        self.__wait_for_nodes_status(UNL_STOPPED, timeout)
        return


class UnlNode(object):
    def __init__(self, unetlab, lab, node_id, path, info):
        self.api = unetlab
        self.lab = lab
        self.id = node_id
        self.path = path + "/"
        self.info = info
        self.name = info["name"]
        url = urlparse.urlparse(info["url"])
        self.proto = url.scheme
        self.ip = url.hostname
        self.port = url.port

    @property
    def status(self):
        data = self.lab.get_node_info(self.id)
        return data["status"]

    @status.setter
    def status(self, value):
        raise UNLApiException("Can't set Node Status")

    def __wait_for_node_status(self, status, timeout):
        stop_time = time.time() + timeout
        while time.time() < stop_time:
            if self.status == status:
                break
            time.sleep(1)
        else:
            raise UNLApiException("Nodes was not started within timeout")
        return

    def start(self, timeout=300):
        resource = self.path + "start"
        self.api.request("GET", resource)
        self.__wait_for_node_status(UNL_RUNNING, timeout)
        return

    def stop(self, timeout=300):
        resource = self.path + "stop"
        self.api.request("GET", resource)
        self.__wait_for_node_status(UNL_STOPPED, timeout)
        return


@contextlib.contextmanager
def unl_lab(server, user, passwd, lab_name):
    unl = UNetLab(server)
    unl.login(user, passwd)
    lab = unl.get_lab(lab_name)
    lab.start_all_nodes()
    yield
    lab.stop_all_nodes()
    unl.logout()
