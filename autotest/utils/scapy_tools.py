#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from scapy.all import *
import struct


def get_pkt_list_intervals(pkts):
    intervals = []
    for i in xrange(len(pkts)-1):
        intervals.append(pkts[i+1].time - pkts[i].time)
    return intervals


def printable_pkts(pkts):
    printable = "-"*80 + "\n"
    for i in xrange(len(pkts)):
        printable += "{} ({})\n".format(i, pkts[i].time)
        printable += "-"*80 + "\n"
        printable += pkts[i].show2(dump=True, indent=2)
        printable += "-"*80 + "\n"
    return printable


def parse_dhcpv4_opt82(pkt):
    opt82 = None
    for option in pkt[DHCP].options:
        if option[0] == "relay_agent_Information":
            opt82 = option[1]
            break
    if opt82 is None:
        return None, None
    cid_length = struct.unpack('!b', opt82[1])[0]
    cid_end = 2 + cid_length
    cid = opt82[2:cid_end]
    rid = opt82[cid_end+2:]
    return cid, rid


def parse_pppoe_vendor_tag(pkt):
    pppoe_tags = pkt[Raw].load
    tags = {}
    i = 0
    while i < len(pppoe_tags):
        tag, length = struct.unpack('!hh', pppoe_tags[i:i+4])
        offset = 4+length
        value = pppoe_tags[i+4:i+offset]
        tags[tag] = value
        if tag == 0:  # End-Of-List
            break
        i += offset
    if 261 not in tags:
        return None, None, None
    option = tags[261]
    vendor_id = option[1:4]
    cid_length = struct.unpack('b', option[5])[0]
    cid_end = 6+cid_length
    cid = option[6:cid_end]
    rid = option[cid_end+2:]
    return vendor_id, cid, rid
