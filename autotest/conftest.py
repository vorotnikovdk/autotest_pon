#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from utils.pyunetlab import UNetLab
import os
import re
import json
import pytest
import tester
import logging

logger = logging.getLogger("conftest")


def pytest_addoption(parser):
  parser.addoption("--cfgfile", action="store", default=None,
                   help="JSON file with Test Session configuration")


def pytest_configure(config):
  """ Load json config and save it in pytest config"""
  json_config = config.getoption("cfgfile")
  if not json_config:
    print("ERROR! '--cfgfile' is required")
    pytest.exit("")
  if not os.path.isfile(json_config):
      print("ERROR! '{}' can not be opened".format(json_config))
      pytest.exit("")
  with open(json_config, "r") as cfg_file:
    config.json_data = json.load(cfg_file)


def suitable_for_dut(test_data, _dut):
  if not re.match(test_data.get("in_hardware", ".*"), _dut["hardware"]):
    return False
  if re.match(test_data.get("ex_hardware", "(?!)"), _dut["hardware"]):
    return False
  if not re.match(test_data.get("in_software", ".*"), _dut["software"]):
    return False
  if re.match(test_data.get("ex_software", "(?!)"), _dut["software"]):
    return False
  return True


@pytest.hookimpl(trylast=True)
def pytest_generate_tests(metafunc):
  if metafunc.cls is None or not hasattr(metafunc.cls, "parameters"):
    return
  _dut = metafunc.config.json_data["devices"]["DUT"]
  for test_data in metafunc.cls.parameters:
    if suitable_for_dut(test_data, _dut):
      params = []
      ids = []
      indirect = test_data.get("indirect", False)
      scope = test_data.get("scope", "function")
      for param in test_data["params"]:
        if suitable_for_dut(param, _dut):
          value = param["value"]
          test_id = param.get("test_id", "")
          for marker in param.get("markers", []):
            value = marker(value)
          params.append(value)
          ids.append(test_id)
      metafunc.parametrize(test_data["fixture"], params, ids=ids,
                           indirect=indirect, scope=scope)


# test server fixtures
@pytest.yield_fixture(scope="session", autouse=True)
def test_server(request):
  """Autofixture creates test server instance"""
  logger.info("Fixture 'test_server' start")
  tester_config = request.config.json_data["tester"]
  _tester = tester.Tester(**tester_config)
  logger.info("Fixture 'test_server' done")
  yield _tester
  logger.info("Fixture 'test_server' teardown")
  _tester.cleanup()
  logger.info("Fixture 'test_server' finished")


@pytest.fixture(scope="class", autouse=True)
def test_server_class(request, test_server):
  """Autofixture sets test class attribute `tester` to current test server"""
  if request.cls is not None:
    logger.debug("{}.tester = test_server".format(request.cls.__name__))
    request.cls.tester = test_server
  return


# Devices fixture
@pytest.yield_fixture(scope="session", autouse=True)
def devices(request, test_server):
  """
  Autofixture creates Devices dict. Keys are names from test_config,
  values are instances of `tester.Device``.
  """
  logger.info("Fixture 'devices' start")
  devs = {}
  devices_config = request.config.json_data["devices"]
  for dev_name, dev in devices_config.iteritems():
    devs[dev_name] = tester.Device(dev, test_server)
  logger.info("Fixture 'devices' done")
  yield devs
  logger.info("Fixture 'devices' teardown")
  for dev in devs.itervalues():
      dev.close()
  logger.info("Fixture 'devices' finished")


@pytest.fixture(scope="class", autouse=True)
def devices_class(request, devices):
  """Autofixture sets test class attribute `devices` to current Devices"""
  if request.cls is not None:
    logger.debug("{}.devices = devices".format(request.cls.__name__))
    request.cls.devices = devices
  return


# DUT fixtures
@pytest.fixture(scope="session", autouse=True)
def dut(request, devices):
  """Autofixture sets `dut` alias for DeviceUnderTest."""
  logger.info("Fixture 'dut' start")
  _dut = devices["DUT"]
  logger.info("Fixture 'dut' done")
  return _dut


@pytest.fixture(scope="class", autouse=True)
def dut_class(request, dut):
  """Autofixture sets test class attribute `dut` to current DeviceUnderTest"""
  if request.cls is not None:
    logger.debug("{}.dut = dut".format(request.cls.__name__))
    request.cls.dut = dut
  return


# Networks fixture
@pytest.fixture(scope="session", autouse=True)
def networks(request, devices, test_server):
  logger.info("Fixture 'networks' start")
  if "networks" not in request.config.json_data:
    logger.info("Fixture 'networks' cancelled")
    return
  networks_config = request.config.json_data["networks"]
  for net_name, net in networks_config.iteritems():
    for device, iface in net.iteritems():
      if device != "tester":
        devices[device].add_network(net_name, iface)
      else:
        test_server.add_network(net_name, iface)
  logger.info("Fixture 'networks' done")
  return


# UNetLan fixtures
@pytest.fixture(scope="session")
def unl(request):
  """Fixture creates UNetLab api instance"""
  logger.info("Fixture 'unl' start")
  unl_config = request.config.json_data["unetlab"]
  _unl = UNetLab(unl_config["host"])
  _unl.login(unl_config["username"], unl_config["password"])
  logger.info("Fixture 'unl' done")
  return _unl


@pytest.yield_fixture()
def unl_lab(request, unl):
  """
  Fixture loads specified UNL lab and starts all nodes.
  After tests it shutdowns nodes.
  Lab name can be pass via pytest.parametrize() with `indirect` param.
  Default scope is `function`, can be changed to `class` with
  pytest.parametrize()'s `scope` param.
  """
  logger.info("Fixture 'unl_lab' start")
  lab = unl.get_lab(request.param)
  lab.start_all_nodes()
  if request.cls is not None:
    logger.debug("{}.unl_lab = unl_lab".format(request.cls.__name__))
    request.cls.unl_lab = lab
  logger.info("Fixture 'unl_lab' done")
  yield lab
  logger.info("Fixture 'unl_lab' teardown")
  lab.stop_all_nodes()
  logger.info("Fixture 'unl_lab' finished")


@pytest.fixture(scope="function", autouse=True)
def process_markers(request):
  logger.info("Process Markers...")
  _dut = request.config.json_data["devices"]["DUT"]
  device_regexs = {}
  for marker in ["in_hardware", "in_software", "ex_hardware", "ex_software"]:
    if marker in request.keywords:
      device_regexs[marker] = request.keywords[marker].args[0]
  if not suitable_for_dut(device_regexs, _dut):
    logger.info("Skipped for '{hardware}' '{software}'".format(**_dut))
    pytest.skip("N/A for '{hardware}' '{software}'".format(**_dut))
  logger.info("Process Markers done")
  return
