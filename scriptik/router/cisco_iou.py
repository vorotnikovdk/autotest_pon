#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from scriptik.device import CLIDevice
import contextlib


class CiscoIOU(CLIDevice):
    """
    Quick and dirty cisco realization for UNL
    """

    # Device
    @contextlib.contextmanager
    def access(self):
        with self.connection("Router"):
            yield

    def get_running_config(self, section=""):
        running = self.run_command("show running-config {}".format(section))
        if running[-1] == "\r":
            return running[:-1]
        return running

    # OSPF
    def set_interface_ospf_timers(self, interface, hello=None, dead=None):
        self.run_command("configure terminal")
        self.run_command("interface {}".format(str(interface)))
        if hello:
            self.run_command("ip ospf hello-interval {}".format(int(hello)))
        if dead:
            self.run_command("ip ospf dead-interval {}".format(int(dead)))
        self.run_command("Router", "exit")
        self.run_command("Router", "exit")
        return

    def get_ospf_neighbors(self):
        resp = self.run_command("show ip ospf neighbor")
        neighbors = []
        for line in resp.split("\r\n")[2:]:
            if line:
                info = line.split()
                neibor = {
                    "router_id": info[0],
                    "address": info[4],
                    "state": info[2]
                }
                neighbors.append(neibor)
        return neighbors
    # END OSPF
