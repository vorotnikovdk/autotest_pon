#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from scriptik.device import *
from scriptik.interfaces.olt import *
from scriptik.interfaces.bootable import *
from scriptik.interfaces.hostname import *
from scriptik.cli.exceptions import *
from contextlib import contextmanager
import re
import time
import logging

logger = logging.getLogger("scriptik.olt")


# noinspection PyPep8Naming
class LTP_3_26_0(CLIDevice, OLT, Hostname, Bootable):
    default_cli = "OLT"

    def __init__(self, onts, **kwargs):
        super(LTP_3_26_0, self).__init__(**kwargs)
        self.onts = onts
        return

    @contextmanager
    def access(self):
        with self.connection("OLT"):
            yield

    def close(self):
        self.close_connections()

    def reboot(self):
        try:
            uncommitted = (r"(?<=You have unsaved changes! Do you really want "
                           r"to reboot the system now\? \(y\/n\)) ", "y")
            no_save = (r"(?<=You have unsaved candidate configuration! Do you "
                       r"want to save it before reboot\? \(y\/n\)) ", "n")
            reboot = (r"(?<=Do you really want to reboot the system now\? "
                      r"\(y\/n\)) ", "y")
            self.run_command("reboot", dialogs=[uncommitted, no_save, reboot])
        except TimeoutExceededException:
            return
        except NetworkException:
            return
        time.sleep(5)  # OLT needs some time to shutdown
        return

    def get_hostname(self):
        info = self.run_command("show management")
        matched = re.search(r"Hostname:\s+'(.*)'", info)
        return matched.group(1)

    def set_hostname(self, hostname):
        self.run_command("configure terminal")
        self.run_command("hostname {}".format(str(hostname)))
        self.run_command("exit")
        self.run_command("commit")
        return

    def olt_ready(self, timeout=300):
        # One OK is not enough cause OLT needs some time to shutdown
        # Lets check two 2 consecutive OKs
        stop_time = time.time() + timeout
        ok = 0
        logger.info("Wait OLT for {} seconds...".format(timeout))
        while time.time() <= stop_time:
            state = self.run_command("show gpon olt state")
            if ("n/a" not in state) and ("not available" not in state):
                ok += 1
            else:
                ok = 0
            if ok > 1:
                break
            time.sleep(5)
        if ok <= 1:
            logger.error("{} OLT not ready".format(self))
            raise RuntimeError("OLT is not available")
        return

    def onts_ready(self, timeout=300):
        max_chan = 7 if "8X" in self.hardware else 3
        conf_resp = self.run_command("show interface ont 0-{} "
                                     "configured".format(max_chan))
        conf_count = re.search(r"Total ONT count: (\d+)", conf_resp).group(1)
        stop_time = time.time() + timeout
        ok = 0
        ok_resp = ''
        ok_count = 0
        logger.info("Wait ONTs for {} seconds...".format(timeout))
        while time.time() <= stop_time:
            ok_resp = self.run_command("show interface ont 0-{} "
                                       "online ok".format(max_chan))
            ok_count = re.search(r"Total ONT count: (\d+)", ok_resp).group(1)
            if ok_count == conf_count:
                ok += 1
            else:
                ok = 0
            if ok > 1:
                break
            time.sleep(5)
        if ok <= 1:
            logger.error("{} ONTS not ready ({}/{})".format(self, ok_count,
                                                            conf_count))
            raise RuntimeError("ONTs are not ready")
        ready_dict = {}
        for name, sn in self.onts.iteritems():
            if sn in ok_resp:
                ready_dict[name] = sn
        return ready_dict

    def onts_configured(self):
        max_chan = 7 if "8X" in self.hardware else 3
        conf_resp = self.run_command("show interface ont 0-{} "
                                     "configured".format(max_chan))
        conf_dict = {}
        for name, sn in self.onts.iteritems():
            if sn in conf_resp:
                conf_dict[name] = sn
        return conf_dict

    def check_ont_state(self, serial):
        try:
            output = self.run_command("show interface ont {} "
                                      "online ok".format(serial))
        except WrongCommandException:
            return False
        if serial in output:
            return True
        else:
            return False

    def get_ont_mac_table(self, serial):
        macs = []
        output = self.run_command("show mac interface ont {}".format(serial))
        for l in output.split('\n')[6:-2]:
            gem = l[14:18].strip() if l[14:18].strip() else None
            uvid = int(l[22:26]) if l[22:26].strip() else None
            cvid = int(l[30:34]) if l[30:34].strip() else None
            svid = int(l[38:42]) if l[38:42].strip() else None
            mac = l[46:].rstrip().lower()
            macs.append(dict(port=gem, mac=mac, uvid=uvid, cvid=cvid, svid=svid))
        return macs

    def fill_interface_id(self, template, ont, mac_record, sn_format='literal',
                          **kwargs):
        opt60 = kwargs['opt60'] if 'opt60' in kwargs else ''
        cid = kwargs['cid'] if 'cid' in kwargs else ''
        rid = kwargs['rid'] if 'rid' in kwargs else ''
        template = template.replace("%MNGIP%", self.connections["OLT"].host)
        template = template.replace("%GPON-PORT%", ont.split('_')[1])
        template = template.replace("%ONTID%", ont.split('_')[-1])
        if sn_format == 'literal':
            sn = self.onts[ont][:8].decode('hex') + self.onts[ont][8:]
        else:
            sn = self.onts[ont]
        template = template.replace("%PONSERIAL%", sn)
        template = template.replace("%GEMID%", mac_record["port"])
        svid = str(mac_record["svid"]) if mac_record["svid"] is not None else '0'
        template = template.replace("%VLAN0%", svid)
        cvid = str(mac_record["cvid"]) if mac_record["cvid"] is not None else '0'
        template = template.replace("%VLAN1%", cvid)
        template = template.replace("%MAC%", mac_record["mac"])
        template = template.replace("%OPT60%", opt60)
        template = template.replace("%OPT82_CID%", cid)
        template = template.replace("%OPT82_RID%", rid)
        return template


# noinspection PyPep8Naming
class LTP_3_26_1(LTP_3_26_0):
    pass
