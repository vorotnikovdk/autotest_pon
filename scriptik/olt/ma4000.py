#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from scriptik.device import *
from scriptik.interfaces.olt import *
from scriptik.interfaces.hostname import *
from scriptik.cli.exceptions import *
from contextlib import contextmanager
import re
import time
import logging

logger = logging.getLogger("scriptik.olt")


# noinspection PyPep8Naming
class MA4000_3_26_0(CLIDevice, OLT, Hostname):
    default_cli = "OLT"

    def __init__(self, onts, **kwargs):
        super(MA4000_3_26_0, self).__init__(**kwargs)
        self.onts = onts
        return

    @contextmanager
    def access(self):
        with self.connection("OLT"):
            yield

    def close(self):
        self.close_connections()

    def get_hostname(self):
        info = self.run_command("show management")
        matched = re.search(r"Hostname:\s+'(.*)'", info)
        return matched.group(1)

    def set_hostname(self, hostname):
        self.run_command("configure terminal")
        self.run_command("hostname {}".format(str(hostname)))
        self.run_command("exit")
        self.run_command("commit")
        return

    def olt_ready(self, timeout=300):
        # One OK is not enough cause OLT needs some time to shutdown
        # Lets check two 2 consecutive OKs
        stop_time = time.time() + timeout
        ok = 0
        logger.info("Wait OLT for {} seconds...".format(timeout))
        while time.time() <= stop_time:
            state = self.run_command("show slot 0-15 gpon olt state")
            if ("n/a" not in state) and ("not available" not in state):
                ok += 1
            else:
                ok = 0
            if ok > 1:
                break
            time.sleep(5)
        if ok <= 1:
            logger.error("{} OLT not ready".format(self))
            raise RuntimeError("OLT is not available")
        return

    def onts_ready(self, timeout=300):
        conf_count = 0
        for slot in xrange(0, 15):
            conf_resp = self.run_command("show interface ont {}/0-7/0-63 "
                                         "configured".format(slot))
            slot_conf_count = re.search(r"total ONT count: (\d+)",
                                        conf_resp).group(1)
            conf_count += int(slot_conf_count)
        stop_time = time.time() + timeout
        ok = 0
        ok_count = 0
        ok_onts_output = ""
        logger.info("Wait ONTs for {} seconds...".format(timeout))
        while time.time() <= stop_time:
            ok_onts_output = ""
            for slot in xrange(0, 15):
                ok_resp = self.run_command("show interface ont {}/0-7/0-63 "
                                           "online ok".format(slot))
                slot_ok_count = re.search(r"total ONT count: (\d+)",
                                          ok_resp).group(1)
                ok_count += int(slot_ok_count)
                ok_onts_output += ok_resp
            if ok_count == conf_count:
                ok += 1
            else:
                ok = 0
            if ok > 1:
                break
            time.sleep(5)
        if ok <= 1:
            logger.error("{} ONTS not ready ({}/{})".format(self, ok_count,
                                                            conf_count))
            raise RuntimeError("ONTs are not ready")
        ready_dict = {}
        for name, sn in self.onts.iteritems():
            if sn in ok_onts_output:
                ready_dict[name] = sn
        return ready_dict

    def onts_configured(self):
        conf_count = 0
        conf_onts_output = ""
        for slot in xrange(0, 15):
            conf_resp = self.run_command("show interface ont {}/0-7/0-63 "
                                         "configured".format(slot))
            slot_conf_count = re.search(r"total ONT count: (\d+)",
                                        conf_resp).group(1)
            conf_count += int(slot_conf_count)
            conf_onts_output += conf_resp
        conf_dict = {}
        for name, sn in self.onts.iteritems():
            if sn in conf_onts_output:
                conf_dict[name] = sn
        return conf_dict

    def check_ont_state(self, serial):
        try:
            output = self.run_command("show interface ont {} "
                                      "online ok".format(serial))
        except WrongCommandException:
            return False
        if serial in output:
            return True
        else:
            return False

    def get_ont_mac_table(self, serial):
        info = self.run_command("show interface ont {} state".format(serial))
        idx = re.search(r"\[ONT(.*)\]", info).group(1)
        output = self.run_command("show mac interface ont {}".format(idx))
        macs = []
        for l in output.split('\r\n')[7:-2]:
            gem = l[9:13].strip() if l[9:13].strip() else None
            uvid = int(l[16:20]) if l[16:20].strip() else None
            cvid = int(l[23:27]) if l[23:27].strip() else None
            svid = int(l[30:34]) if l[30:34].strip() else None
            mac = l[37:].rstrip().lower()
            macs.append(dict(port=gem, mac=mac, uvid=uvid, cvid=cvid, svid=svid))
        return macs

    def fill_interface_id(self, template, ont, mac_record, sn_format='literal',
                          **kwargs):
        opt60 = kwargs['opt60'] if 'opt60' in kwargs else ''
        cid = kwargs['cid'] if 'cid' in kwargs else ''
        rid = kwargs['rid'] if 'rid' in kwargs else ''
        template = template.replace("%MNGIP%", self.connections["OLT"].ip)
        template = template.replace("%SLOT%", ont.split('_')[0])
        template = template.replace("%GPON-PORT%", ont.split('_')[1])
        template = template.replace("%ONTID%", ont.split('_')[-1])
        if sn_format == 'literal':
            sn = self.onts[ont][:8].decode('hex') + self.onts[ont][8:]
        else:
            sn = self.onts[ont]
        template = template.replace("%PONSERIAL%", sn)
        template = template.replace("%GEMID%", mac_record["port"])
        svid = str(mac_record["svid"]) if mac_record["svid"] is not None else '0'
        template = template.replace("%VLAN0%", svid)
        cvid = str(mac_record["cvid"]) if mac_record["cvid"] is not None else '0'
        template = template.replace("%VLAN1%", cvid)
        template = template.replace("%MAC%", mac_record["mac"])
        template = template.replace("%OPT60%", opt60)
        template = template.replace("%OPT82_CID%", cid)
        template = template.replace("%OPT82_RID%", rid)
        return template
