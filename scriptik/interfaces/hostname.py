#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
import abc


class Hostname(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def get_hostname(self):
        """
        Returns current hostname.

        :return: string, hostname
        """
        return

    @abc.abstractmethod
    def set_hostname(self, hostname):
        """
        Sets new hostname.

        :param hostname: string, new hostname
        :return:
        """
        return
