#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
import abc


class OLT(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def olt_ready(self, timeout=300):
        """
        Returns right after olt is ready to work. If it is not ready after
        specified `timeout` it raises RuntimeError.

        :param timeout: int, time to wait in seconds
        :raises RuntimeError if OLT is not ready
        :return:
        """
        return

    @abc.abstractmethod
    def onts_ready(self, timeout=300):
        """
        Returns right after all ONTs is ready to work. If they are not ready
        after specified `timeout` it raises RuntimeError.

        :param timeout: int, time to wait in seconds
        :raises RuntimeError if any ONT is not ready
        :return: dict, same as `self.onts` with onts that are online
        """
        return

    @abc.abstractmethod
    def check_ont_state(self, serial):
        """
        Returns is ont ready or not.

        :param serial: string, serial number of target ONT
        :return: boolean, True if online ok, False otherwise
        """
        return

    @abc.abstractmethod
    def onts_configured(self):
        """
        ONTs that exist in OLT configuration.

        :return: dict same as `self.onts` with onts that are configured
        """
        return

    @abc.abstractmethod
    def get_ont_mac_table(self, serial):
        """
        Returns MAC table for ONT.

        :param serial: string, serial number of target ONT
        :return: list of dicts with keys `mac`, `port` (which is GEM),
                `uvid', `svid`, `cvid`
        """
        return

    @abc.abstractmethod
    def fill_interface_id(self, template, ont, mac_record, **kwargs):
        """
        Fill passed `template` with `ont` data from configuration and passed
        `mac record`.

        :param template: interface ID template
        :param ont: ont name from `OLT.onts`
        :param mac_record: one record from `OLT.get_ont_mac_table()`
        :param kwargs: any other params
        :return: filled interface ID
        """
