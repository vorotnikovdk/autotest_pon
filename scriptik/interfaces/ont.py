#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
import abc


class ONT(object):
    __metaclass__ = abc.ABCMeta
    pass
