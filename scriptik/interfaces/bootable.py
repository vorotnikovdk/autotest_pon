#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
import abc


class Bootable(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def reboot(self):
        """
        Reboot device right now.

        :return:
        """
        return
