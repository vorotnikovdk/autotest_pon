#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from scriptik.device import Device
from scriptik.interfaces.ont import ONT


# noinspection PyPep8Naming
class NTU1402_3_24_3(Device, ONT):
    pass
