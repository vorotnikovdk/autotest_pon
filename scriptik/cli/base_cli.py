#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
import re
import socket
import logging
import Exscript
from StringIO import StringIO
from scriptik.cli.exceptions import *
from Exscript.protocols.Telnet import Telnet
from Exscript.protocols.SSH2 import SSH2
from Exscript.protocols.Exception import ProtocolException
from Exscript.protocols.Exception import TimeoutException

logger = logging.getLogger("scriptik.cli")


def check_connected(method):
    """
    Decorator that checks if CLIConnection was connected and authorized

    :param method: func, method to decorate
    :raises: NotConnectedError if not connected
    :return: func, decorated method
    """
    def checked(self, *args, **kwargs):
        if self.conn is None:
            raise NotConnectedError("{} is not connected!".format(self))
        return method(self, *args, **kwargs)
    return method


class CliConnection(object):
    driver = None

    def __init__(self, proto, host, port=None, login=None, password=None,
                 enable_password=None, is_serial=False, debug=0):
        """
        Create CLIConnection object.

        :param proto: connect protocol: "telnet" or "ssh"
        :param host: string, ip address to connect
        :param port: int, tcp port to connect
        :param login: string, login to log in; if None - auth skipped
        :param password: string, password to log in; if None - auth skipped
        :param enable_password: string, app authorize password (see Exscript)
        :param is_serial: bollean, if True - send '\r\n' after connect
        :param debug: int, Exscript debug value
        :return:
        """
        self.proto = proto
        self.host = host
        if port is not None:
            self.port = port
        elif proto == "telnet":
            self.port = 23
        elif proto == "ssh":
            self.port = 22
        self.login = login
        self.password = password
        self.enable_password = enable_password
        self.is_serial = is_serial
        self.conn = None
        self.stdout = StringIO()
        self.debug = debug
        assert self.driver is not None, "Missing driver for {}".format(self)
        logger.debug("Created {}".format(self))
        return

    def __repr__(self):
        state = "Active " if self.conn is not None else ""
        return "{}{} connection to {}:{} <{}>".format(state, self.proto,
                                                      self.host, self.port,
                                                      id(self))

    def is_connected(self):
        """
        Check if CLI was connected.

        :return: boolean state
        """
        return self.conn is not None

    def connect(self, tries=1, connect_timeout=5):
        """
        Create Exscript protocol instance, connect, authorize and init terminal.

        :param tries: int, how many tries will be done before error
        :param connect_timeout: int, time in seconds to wait connection
        :return:
        """
        logger.info("Connecting: {}".format(self))
        if self.conn is not None:
            logger.debug("Already connected: {}".format(self))
            return
        if self.proto == "telnet":
            self.conn = Telnet(debug=self.debug,
                               connect_timeout=connect_timeout,
                               stdout=self.stdout)
        elif self.proto == "ssh":
            self.conn = SSH2(debug=self.debug,
                             connect_timeout=connect_timeout,
                             stdout=self.stdout)
        else:
            err_msg = "Incorrect protocol: '{}' for {}".format(self.proto, self)
            raise ValueError(err_msg)
        self.conn.set_driver(self.driver)
        current_tries = 1
        while current_tries <= tries:
            try:
                logger.debug("Trying to connect {}: {}".format(current_tries,
                                                               self))
                self.conn.connect(self.host, self.port)
                break
            except socket.error:
                current_tries += 1
        else:
            self.conn = None
            logger.error("Cannot connect: {}".format(self))
            raise
        logger.debug("Connected: {}".format(self))
        if self.is_serial:
            self.conn.send("\r\n")
        if self.login is None or self.password is None:
            logger.info("Auth skipped for {}".format(self))
        else:
            if self.enable_password is not None:
                account = Exscript.Account(self.login, password=self.password,
                                           password2=self.enable_password)
            else:
                account = Exscript.Account(self.login, password=self.password)
            self.conn.login(account)
            logger.debug("Authorized: {}".format(self))
        self.conn.autoinit()
        logger.debug("Done: {}".format(self))
        return

    def disconnect(self):
        """
        Correctly shutdown the CLI connection. May require executing some
        command such a 'exit', so can be overwritten. After disconnect
        `self.conn` must be None.

        :return:
        """
        logger.info("Disconnecting: {}".format(self))
        if self.conn is None:
            logger.debug("Already disconnected: {}".format(self))
            return
        try:
            self.conn.close(force=True)
        except TimeoutException:
            pass
        self.conn = None
        logger.debug("Disconnected: {}".format(self))
        return

    def read_log(self, pos=0):
        """
        Read CLI output.

        :param pos: int, initial position. 0 means all from beginning
        :return: string containing CLI log
        """
        self.stdout.seek(pos)
        return self.stdout.read()

    def clear_log(self, pos=0):
        """
        Flush CLI output.

        :param pos: int, initial position. 0 means all from beginning
        :return:
        """
        self.stdout.seek(pos)
        self.stdout.truncate()
        return

    @check_connected
    def get_prompt(self):
        """
        Reads current prompt.

        :return: string, current prompt
        """
        self.conn.send('\r\n')
        _, prompt_match = self.conn.expect_prompt()
        return prompt_match.group().split('\n')[-1]

    def set_timeout(self, timeout):
        """
        Sets timeout to wait the prompt or awaited string/regex.

        :param timeout: int, timeout in seconds
        :return: int, old timeout value in seconds.
        """
        old_timeout = self.conn.get_timeout()
        self.conn.set_timeout(timeout)
        return old_timeout

    @check_connected
    def execute(self, command, dialogs=None, timeout=None, send_cb=None):
        """
        Send `command` into CLI and waits for prompt. Timeout can be customized
        for executed command. Other expected strings and commands for them can
        be specified via `dialogs`. If `driver` has `strip_log_lines` method,
        all matched strings also will be stripped.
        Right after sending command `send_cb` callback can be executed.

        :param command: string, command to execute
        :param dialogs: list of tuples of strings (expected, answer to send)
        :param timeout: int, timeout in seconds for this `command`
        :param send_cb: func, callback executed after sending `command`
        :raises: WrongCommandException if error prompt found
        :raises: TimeoutExceededException if no prompt found
        :return: string, all read data except prompt or last empty line
        """
        if timeout is not None:
            old_timeout = self.set_timeout(timeout)
        else:
            old_timeout = None
        ext_prompts = []
        if dialogs is not None:
            for pattern, _ in dialogs:
                ext_prompts.append(re.compile(pattern))
        ext_prompts.extend(self.conn.get_prompt())
        self.conn.send(command + '\r')
        if send_cb is not None:
            logger.debug("Executing callback: {}".format(self))
            send_cb()
        result = ""
        while True:
            try:
                index, match = self.conn.expect(ext_prompts)
            except TimeoutException:
                err_msg = "No prompt after command '{}': {}".format(command,
                                                                    self)
                logger.error(err_msg)
                raise TimeoutExceededException(err_msg)
            except ProtocolException:
                err_msg = "Proto error after command '{}': {}".format(command,
                                                                      self)
                logger.error(err_msg)
                raise NetworkException(err_msg)
            result += self.conn.response
            if dialogs is not None and index < len(dialogs):
                self.conn.send("{}\r".format(dialogs[index][1]))
                ext_prompts.pop(index)
                dialogs.pop(index)
            else:
                break
        if hasattr(self.driver, "strip_log_lines"):
            result = self.driver.strip_log_lines(result)
        prompts = self.conn.get_prompt()
        # Exscript SSH2 leaves prompt in response. Remove it
        if self.proto == "ssh":
            for p in prompts:
                match = p.search(result)
                if match:
                    result = re.sub(match.group(0), "", result)
        result_lines = result.split("\n")[1:]
        # Exscript Telnet leaves empty line in response. Remove it
        if self.proto == "telnet":
            if len(result_lines) and result_lines[-1] == "":
                result_lines.pop()
        result = "\n".join(result_lines)
        for line in result_lines:
            for prompt in self.conn.get_error_prompt():
                if not prompt.search(line):
                    continue
                args = repr(prompt.pattern), repr(line)
                # Add lines to Exscript debug
                # noinspection PyProtectedMember
                self.conn._dbg(5, "error prompt (%s) matches %s" % args)
                raise WrongCommandException('Device said:\n' + result)
        if old_timeout:
            self.set_timeout(old_timeout)
        return result

    @check_connected
    def send(self, command):
        """
        Passed to Exscript.

        :param command: string, command to send
        :return:
        """
        return self.conn.send(command)

    @check_connected
    def expect_prompt(self):
        """
        Passed to Exscript.

        :return:
        """
        return self.conn.expect_prompt()

    @check_connected
    def expect(self, prompt):
        """
        Passed to Exscript.

        :param prompt: regex or regex list to wait in response
        :return: string, read data
        """
        return self.conn.expect(prompt)
