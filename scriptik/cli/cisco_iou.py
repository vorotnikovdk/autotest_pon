#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from scriptik.cli.base_cli import CliConnection
from Exscript.protocols.drivers.ios import IOSDriver


class CiscoIOUCli(CliConnection):
    driver = IOSDriver()

