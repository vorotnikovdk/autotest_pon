#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals


class NotConnectedException(Exception):
    """CLIConnection was not connected and authorized"""
    pass


class WrongCommandException(Exception):
    """CLIConnection returns error prompt"""
    pass


class TimeoutExceededException(Exception):
    """CLIConnection can not find prompt within timeout"""
    pass


class NetworkException(Exception):
    """CLIConnection can not find prompt within timeout"""
    pass
