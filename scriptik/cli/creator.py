#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from scriptik.cli.olt import *
from scriptik.cli.cisco_iou import *
from scriptik.cli.vpc import *


def create_cli(cli_type, hardware, software, cli_config):
    if cli_type == "VPC":
        return VPCCli(**cli_config)
    if cli_type == "OLT":
        if "LTP" or "MA4000" in hardware:
            if software.startswith("3"):
                return LTP3XClish(**cli_config)