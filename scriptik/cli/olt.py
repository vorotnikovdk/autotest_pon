#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from scriptik.cli.exscript_drivers import *
from scriptik.cli.base_cli import CliConnection
from scriptik.cli.exceptions import *
import socket
import logging

logger = logging.getLogger("scriptik.cli")


class LTP3XClish(CliConnection):
    driver = OLTClishDriver()

    def disconnect(self):
        logger.debug("Prepare disconnecting: {}".format(self))
        self.set_timeout(10)
        while True:
            try:
                uncommitted = (r"(?<=You have unsaved changes!Do you really "
                               r"want to exit CLI\? \(y\/n\)) ", "y")
                no_save = (r"(?<=You have unsaved candidate configuration! Do "
                           r"you want to save it before exit CLI\? \(y\/n\)) ",
                           "n")

                self.execute("exit", dialogs=[uncommitted, no_save])
            except TimeoutExceededException:
                break
            except NetworkException:
                break
            except EOFError:
                break
            except socket.error:
                break
        super(LTP3XClish, self).disconnect()
        return


class LTP3XLinux(CliConnection):
    driver = OLTLinuxDriver()

    def disconnect(self):
        logger.debug("Prepare disconnecting: {}".format(self))
        self.conn.set_timeout(10)
        while True:
            try:
                self.execute("exit")
            except Exscript.protocols.Exception.ProtocolException:
                break
        super(LTP3XLinux, self).disconnect()
        return
