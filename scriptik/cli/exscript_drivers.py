#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
import re
from Exscript.protocols.drivers.driver import Driver
from Exscript.protocols.Exception import InvalidCommandException


class OLTClishDriver(Driver):
    def __init__(self):
        Driver.__init__(self, "OLTClishDriver")
        self.user_re = [re.compile(r"login: ")]
        self.password_re = [re.compile(r"Password: $")]
        self.prompt_re = [re.compile(r"[\r\n]?\S+\(?\S*\)?# ?$"),
                          re.compile(r"[\r\n]?\(acs-?.*\)$")]
        clish_errors = [r"error",
                        r"invalid",
                        r"Revision failed",
                        r"unknown command",
                        r"connection timed out",
                        r"the command is not completed",
                        r"[^\r\n]+ not found"]
        self.error_re = [re.compile(r"^%?\s*(?:" + "|".join(clish_errors) +
                                    r")", re.I)]
        self.login_error_re = [
            re.compile(r"[\r\n]?\s*(?:incorrect|available cli)")]
        self.log_re = [re.compile(r"(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|"
                                  r"Nov|Dec)\s+\d{1,2}\s(?:\d\d:?){3}\s.*:.*\n")
                       ]
        return

    def strip_log_lines(self, response):
        cleaned_response = response
        for logging_re in self.log_re:
            cleaned_response = logging_re.sub('', cleaned_response)
        return cleaned_response


class OLTLinuxDriver(OLTClishDriver):
    def __init__(self):
        Driver.__init__(self, "OLTLinuxDriver")
        self.user_re = [re.compile(r"login: ")]
        self.password_re = [re.compile(r"password:", re.I)]
        self.prompt_re = [re.compile(r"[\r\n]?\[?\S* \S*\]?>?[$#] ?")]
        self.log_re = [re.compile(r"(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|"
                                  r"Nov|Dec)\s+\d{1,2}\s(?:\d\d:?){3}\s.*:.*\n")
                       ]

    def auto_authorize(self, conn, account, flush, bailout):
        conn.send("su\r")
        conn.app_authorize(account, flush, bailout)

    def strip_log_lines(self, response):
        cleaned_response = response
        for logging_re in self.log_re:
            cleaned_response = logging_re.sub('', cleaned_response)
        return cleaned_response


class VPCDriver(Driver):
    def __init__(self):
        Driver.__init__(self, "OLTLinuxDriver")
        self.prompt_re = [re.compile(r"> ")]
        self.error_re = [re.compile(r"Bad command"),
                         re.compile(r"Invalid")]

    def init_terminal(self, conn):
        try:
            conn.expect_prompt()
        except InvalidCommandException:
            pass
