#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from scriptik.cli.base_cli import CliConnection
from scriptik.cli.exscript_drivers import *


class VPCCli(CliConnection):
    driver = VPCDriver()
