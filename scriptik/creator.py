#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from scriptik.olt.ltp import *
from scriptik.olt.ma4000 import *
from scriptik.ont.ntu import *
from scriptik.vpc import *


def create_device(device_config):
    if "hardware" not in device_config or "software" not in device_config:
        err_msg = "Missing 'hardware' or 'software' section in config"
        raise ValueError(err_msg)
    if "LTP" in device_config["hardware"]:
        if device_config["software"] == "3.26.0":
            return LTP_3_26_0(**device_config)
        if device_config["software"] == "3.26.1":
            return LTP_3_26_1(**device_config)
    if "MA4000" in device_config["hardware"]:
        if device_config["software"] == "3.26.0":
            return MA4000_3_26_0(**device_config)
    if "NTU1402" in device_config["hardware"]:
        if device_config["software"] == "3.24.3":
            return NTU1402_3_24_3(**device_config)
    if "VPC" == device_config["hardware"]:
        return VPC(**device_config)
    err_msg = "Unknown device '{}' or software version: '{}'"
    err_msg = err_msg.format(device_config["hardware"],
                             device_config["software"])
    raise ValueError(err_msg)
