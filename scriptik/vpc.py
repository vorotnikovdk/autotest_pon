#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from scriptik.device import CLIDevice
import re
import contextlib


class VPC(CLIDevice):
    default_cli = "VPC"

    @contextlib.contextmanager
    def access(self):
        with self.connection("VPC"):
            yield

    def dhcp(self):
        out = self.run_command("dhcp")
        if "Can't find dhcp server" in out:
            return False
        else:
            return re.search(r"IP (.*)/", out).group(1)

    def ping(self, ip):
        out = self.run_command("ping {}".format(ip))
        if "timeout" in out or "not reachable" in out:
            return False
        else:
            return True
