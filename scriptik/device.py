#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from scriptik.cli.creator import create_cli
import os
import logging
from contextlib import contextmanager

logger = logging.getLogger("scriptik.device")


class Device(object):

    def __init__(self, hardware, software, **kwargs):
        super(Device, self).__init__()
        self.hardware = hardware
        self.software = software
        return

    @contextmanager
    def access(self):
        """
        Context manager that starts all cli connection, web sessions or whatever
        is needed to work with device. At __exit__ it correctly exits.
        Must be overwritten.
        """
        yield

    def close(self):
        """
        Close all connections and teardown Device.
        Must be overwritten.
        :return:
        """
        return


class CLIDevice(Device):
    default_cli = None

    def __init__(self, cli_configs, **kwargs):
        """
        Create all predefined CLIs but do not start it.

        :param cli_configs: list of dicts, params for CLIConnections
        :param kwargs: other keyword arguments
        :return:
        """
        super(CLIDevice, self).__init__(**kwargs)
        self.cli_configs = cli_configs
        self.connections = {}
        for cli_type, cli_config in cli_configs.iteritems():
            cli = create_cli(cli_type, self.hardware, self.software, cli_config)
            self.connections[cli_type] = cli
        return

    def __repr__(self):
        return "{} {} <{}>".format(self.hardware, self.software, id(self))

    @contextmanager
    def connection(self, cli_type, copy=True, tries=1, connect_timeout=5,
                   **kwargs):
        """
        Context manager that create and connect new CLIConnection. If `copy`
        is True new connection uses all params from predefined CLI, else it uses
        Connection defaults.
        All params can be overwritten via keyword arguments.
        After work it correctly exits the CLI.

        :param cli_type: string, type of predefined CLIConnection
        :param copy: boolean, use predefined CLI params instead of defaults
        :param tries: int, how many tries will be done before error
        :param connect_timeout: int, time in seconds to wait connection
        :param kwargs: params for CLIConnection constructor
        :return:
        """
        cli_config = self.cli_configs[cli_type] if copy else {}
        for param, value in kwargs:
            cli_config[param] = value
        cli = create_cli(cli_type, self.hardware, self.software, cli_config)
        cli.connect(tries=tries, connect_timeout=connect_timeout)
        yield cli
        cli.disconnect()

    def run_command(self, command, con=None, cli_type=None, disconnect=False,
                    **kwargs):
        """
        Execute passed command in specified CLIConnection, predefined `cli_type`
        CLIConnection or `default_cli` CLIConnection.
        If both of `con` and `cli_type` passed method uses `con`.
        If CLIConnection is not connected, it will try to connect.
        Other parameters are passed to CLIConnection's `execute` method.

        :param command: string, command to execute
        :param con: CLIConnection to be used
        :param cli_type: string, type of predefined CLIConnection to be used
        :param disconnect: boolean, disconnect CLIConnection after execute
        :param kwargs: params for CLIConnection `execute`
        :raises: WrongCommandException if error prompt found
        :return: string, response from CLIConnection's `execute`
        """
        if con is not None:
            con = con
        elif cli_type is not None:
            con = self.connections[cli_type]
        else:
            con = self.connections[self.default_cli]
        if not con.is_connected():
            con.connect()
        response = con.execute(command, **kwargs)
        if disconnect:
            con.disconnect()
        return response

    def run_commands_batch(self, commands, con=None, cli_type=None,
                           disconnect=False, **kwargs):
        """
        Execute all commands in list one by one in same CLIConnection with same
        parameters.

        :param commands: list of strings, commands to execute
        :param con: CLIConnection to be used
        :param cli_type: string, type of predefined CLIConnection to be used
        :param disconnect: boolean, disconnect CLIConnection after last execute
        :param kwargs: params for CLIConnection `execute`
        :raises: WrongCommandException if error prompt found
        :return: list of string, responses from CLIConnection's `execute`
        """
        if con is not None:
            con = con
        elif cli_type is not None:
            con = self.connections[cli_type]
        else:
            con = self.connections[self.default_cli]
        if not con.is_connected():
            con.connect()
        responses = []
        for command in commands:
            responses.append(con.execute(command, **kwargs))
        if disconnect:
            con.disconnect()
        return responses

    def run_commands_from(self, readable, con=None, cli_type=None,
                          disconnect=False, **kwargs):
        """
        Execute all commands line by line from passed opened file or other
        readable object in same CLIConnection with same parameters.
        Empty lines are skipped.

        :param readable: opened object with read() method
        :param con: CLIConnection to be used
        :param cli_type: string, type of predefined CLIConnection to be used
        :param disconnect: boolean, disconnect CLIConnection after last execute
        :param kwargs: params for CLIConnection `execute`
        :raises: WrongCommandException if error prompt found
        :return: list of string, responses from CLIConnection's `execute`
        """
        commands = readable.read().split(os.linesep)
        return self.run_commands_batch(commands, con=con, cli_type=cli_type,
                                       disconnect=disconnect, **kwargs)

    def execute(self, cli_type, command, dialogs=None, timeout=None,
                send_cb=None):
        """ Deprecated. Run command in predefined `cli_type`.

        :param cli_type: string, type of predefined CLIConnection to be used
        :param command: string, command to execute
        :param dialogs: list of tuples of strings (expected, answer to send)
        :param timeout: int, timeout in seconds for this `command`
        :param send_cb: func, callback executed after sending `command`
        :raises: WrongCommandException if error prompt found
        :raises: TimeoutExceededException if no prompt found
        :return: string, all read data except prompt or last empty line
        """
        connection = self.connections[cli_type]
        if not connection.is_connected():
            raise RuntimeError("{}: CLI was not "
                               "connected".format(connection.host))
        return connection.execute(command, dialogs=dialogs, timeout=timeout,
                                  send_cb=send_cb)

    def close_connections(self):
        """
        Disconnect all predefined CLIConnections

        :return:
        """
        for conn in self.connections.itervalues():
            conn.disconnect()
