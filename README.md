Common autotests code
=====================

Requires:

* python 2.7

* tcpdump (4.5.1)

* iptables (1.4.21)

* unetlab (1.0.0-4)

* Exscript (https://github.com/knipknap/exscript), tested on 2.1.503

* scapy (https://github.com/secdev/scapy), tested on 2.3.2.dev365

* pytest (https://pypi.python.org/pypi/pytest), tested on 2.9.0

* pytest-allure-adaptor (https://pypi.python.org/pypi/pytest-allure-adaptor), tested on 1.7.4

* allure-commandline (https://github.com/allure-framework/allure-debian), tested on 1.4.23-2

* PyHamcrest (https://github.com/hamcrest/PyHamcrest), tested on 1.9.0